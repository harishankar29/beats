<?php

use App\Models\Traits\SchemaHelper;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AnnounceClass extends Migration
{
    use SchemaHelper;
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Announce_Classes', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('announce_id')->length(10)->nullable();
            $table->unsignedBigInteger('class_id')->length(10)->nullable();
            $this->activityFields($table);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Announce_Classes');
    }
}
