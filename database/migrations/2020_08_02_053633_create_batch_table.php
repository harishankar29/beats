<?php

use App\Models\Traits\SchemaHelper;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBatchTable extends Migration
{
     use SchemaHelper;
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('batches', function (Blueprint $table) {
            $table->id();
            $table->string('batch')->length(15)->nullable();
            $table->string('class')->length(10)->nullable();
            $table->string('subject')->length(20)->nullable();
            $table->string('center')->length(20)->nullable();
            $table->string('date')->length(20)->nullable();
            $table->string('start_time')->length(20)->nullable();
            $table->string('end_time')->length(20)->nullable();
            $table->string('weekday')->length(20)->nullable();
            $table->string('created_time')->length(20)->nullable();
            $this->activityFields($table);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('batches');
    }
}
