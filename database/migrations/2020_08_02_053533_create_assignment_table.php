<?php


use App\Models\Traits\SchemaHelper;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAssignmentTable extends Migration
{

    use SchemaHelper;
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assignments', function (Blueprint $table) {
            $table->id();
            $table->string('assignment')->length(100)->nullable();
            $table->string('topic')->length(50)->nullable();
            $table->string('submission_date')->length(100)->nullable();
            $table->string('attachement')->length(100)->nullable();
            $table->string('classe')->length(50)->nullable();
            $table->string('attach_name')->length(50)->nullable();
            $this->activityFields($table);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assignments');
    }
}
