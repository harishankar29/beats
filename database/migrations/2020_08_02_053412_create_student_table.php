<?php

use App\Models\Traits\SchemaHelper;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;


class CreateStudentTable extends Migration
{
    use SchemaHelper;
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {
            $table->id();
            $table->string('name')->length(20)->nullable();
            $table->string('class')->length(10)->nullable();
            $table->string('school')->length(20)->nullable();
            $table->string('profile_pic')->length(100)->nullable();
            $table->string('batch')->length(10)->nullable();
            $table->string('address')->length(100)->nullable();
            $table->string('father')->length(50)->nullable();
            $table->string('mother')->length(50)->nullable();
            $table->string('father_contact')->length(50)->nullable();
            $this->activityFields($table);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students');
    }
}
