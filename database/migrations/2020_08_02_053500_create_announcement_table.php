<?php


use App\Models\Traits\SchemaHelper;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAnnouncementTable extends Migration
{

    use SchemaHelper;
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('announcements', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id')->nullable();
            $table->string('announcement')->length(50)->nullable();
            $table->string('image')->length(100)->nullable();
            $table->string('batch')->length(10)->nullable();
            $table->string('attach')->length(50)->nullable();
            $table->string('class')->length(10)->nullable();
            $this->activityFields($table);
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('announcements');
    }
}
