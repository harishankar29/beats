<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes(['verify' => true]);


Route::group(['middleware' => ['auth','verified']], function(){
    Route::get('/home', 'HomeController@index')->name('home');
});


/*View Routes*/
Route::get('/' , 'ViewController@home')->name('home');
Route::get('/about' , 'ViewController@about')->name('about');
Route::get('/contact' , 'ViewController@contact')->name('contact');
Route::get('/course-detail' , 'ViewController@detail')->name('detail');
Route::get('/courses' , 'ViewController@courses')->name('courses');



