const mix = require('laravel-mix');

/* 
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

// Compile Section
mix.js('resources/js/apps/admin/app.js', 'public/js/admin.js')
    .js('resources/js/apps/student/app.js', 'public/js/student.js')
    .sass('resources/sass/app.scss', 'public/css')
    .sass('resources/sass/admin.scss','public/css')
    .sass('resources/sass/student.scss','public/css');

// Plugins Copy From Node Modules
mix.copy('node_modules/admin-lte/dist','public/admin-lte/dist')
   .copy('node_modules/admin-lte/plugins','public/admin-lte/plugins')
   .copy('node_modules/tabler-ui/dist/assets', 'public/js/plugins/tabler')
   .copy('node_modules/admin-lte/plugins/fontawesome-free', 'public/js/plugins/fontawesome-free')
   .copy('node_modules/tabler-ui/dist/demo', 'public/img')
   .copy('node_modules/select2/dist' , 'public/select2');

// Copy Directory
mix.copyDirectory('resources/pictures' , 'public/pictures')
	.copyDirectory('resources/vendors' , 'public/vendors')
    .copyDirectory('resources/jq' , 'public/jq')
    .copyDirectory('resources/fonts' , 'public/fontawesome');