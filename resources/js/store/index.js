import Vue from 'vue'
import Vuex from 'vuex'
import modules from './modules'
Vue.use(Vuex)

export default new Vuex.Store({
	modules,

	state: {
	    error : null,
	    errorCode:null
	},

	mutations: {
	   setError(state,error){
	   	state.error = error;
	   }
	},

	actions:{

	},

	getters:{
		getError: state => state.error,
		getErrorCode : state=> state.errorCode
	}
})

