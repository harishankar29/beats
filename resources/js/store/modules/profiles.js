export default{
    state:{
        loading: false,
        profiles:[],
        profile:{
            name : '',
            password : ''
        }
    },

    mutations:{
        SET_USERS(state , profile){
            state.profiles = profile;
        }
    },

    actions:{
        async submitProfile({commit , state},payload){
            try{
                 await Axios.post('/users' , payload);
                            
            }catch(error){
                return Promise.reject(error);
            }finally{

            }
        },

        async listProfile({commit , state}){
            try{
                let response = await Axios.get('/users');
                commit("SET_USERS" , response.body.items);
                return true;
            }
            catch(error){
                return Promise.reject(error);
            }finally{

            }

        }
    },

    getters:{

    }

}