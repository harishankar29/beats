import Axios from "axios";

export default {
    state:{
        loading:false,
        pagination:null,
        subjects:[],
        subject:{
            class   : '',
            subject : '',
            faculty : ''
        }
    },

    mutations:{

        SET_LOADING(state , loading){
            state.loading = loading;
        },

        SET_SUBJECTS(state , subject){
            state.subjects = subject;
        },

        SET_SUBJECT(state , subject){
            state.subject = subject;
        },

        SET_PAGINATION(state, pagination){
			state.pagination = pagination;
		}
    },

    actions:{

        // SAVE FACULTY
        async saveSubject({commit , state} , payload){
            try{
                commit("SET_LOADING" , true);
                let response = await Axios.post('/subjects' , payload);
                return true;
            }catch(error){
                return Promise.reject(error);
            }finally{
                commit("SET_LOADING" , false);
            }
        },

        // LIST FACULTY
        async listSubjects({commit , state} , query){
            try{
                commit("SET_LOADING" , true);
                let response = await Axios.get('/subjects' + query);
                commit("SET_SUBJECTS" , response.body.items);
                commit('SET_PAGINATION', response.body.pagination ? response.body.pagination : null);
                return true;
            }catch(error){
                return Promise.reject(error);
            }finally{
                commit("SET_LOADING" , false);
            }
        },

        // EDIT STUDENT
		async editSubject({commit , state} , params){
			try{
				commit("SET_LOADING" , true);
				let response = await Axios.get('/subjects/'+ params);
				commit("SET_SUBJECT" , response.body);
				return true;
			}catch(error){
				return Promise.reject(error);
			}finally{
				commit("SET_LOADING" , false);
			}
		},

		// UPDATE STUDENT
		async updateSubject({commit , state} , payload ){
			try{
				commit("SET_LOADING" , true);
				let response =  await Axios.put('/subjects/'+ payload.id , payload);
				commit("SET_SUBJECTS" , response.body.items);
				return true;
			}catch(error){
				return Promise.reject(error);
			}finally{
				commit("SET_LOADING" , false);
			}
		}, 



        // DELETE SUBJECTS
        async deleteSubject({commit , state} , params , query){
            if(confirm("Do you really want to delete?")){
                try{
                    commit("SET_LOADING" , true);
                    let response = await Axios.delete('/subjects/' + params + query);
                    commit("SET_SUBJECTS" , response.body.items);
                    commit('SET_PAGINATION', response.body.pagination ? response.body.pagination : null);
                }catch(error){
                    return Promise.reject(error);
                }finally{
                    commit("SET_LOADING" , false);
                }
            }
        }
    },

    getters:{

    }
}