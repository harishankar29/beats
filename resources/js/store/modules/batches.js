export default {
	state:{
		loading : false,
		pagination : null,
		batches:[],
		batch:{

			batch : '',
			class : '',
			subject: '',
			center : '',
			date   : '',
			start_time : '',
			end_time : '',
			weekday : [],

		}
	},

	mutations:{

		SET_LOADING(state ,loading){
			state.loading = loading
		},

		SET_BATCHES(state , data){
			state.batches = data;
		},

		SET_BATCH(state , batch){
			state.batch = batch;
		},

		SET_PAGINATION(state , pagination){
			state.pagination = pagination;
		}

	},

	actions:{

		// SAVE BATCHES
		async saveBatches({commit , state} , payload){
			try{
				commit ("SET_LOADING", true);
				let response = await Axios.post('/batches' , payload);
				return true;
			}catch(error){
				return Promise.reject(error);
			}finally{
				commit("SET_LOADING",false);
			}
		},

		// LIST BATCHES
		async listBatches({commit , state} , query){
			try{
				commit("SET_LOADING",true);
				let response = await Axios.get('/batches'+query);
				commit("SET_BATCHES", response.body.items);
				commit("SET_PAGINATION" , response.body.pagination ? response.body.pagination : null);
				return true;
			}catch(error){
				return Promise.reject(error);
			}finally{
				commit("SET_LOADING", false);
			}
		},

		// EDIT BATCHES
		async editBatches({commit , state},params){
			try{
				commit("SET_LOADING",true);
				let response = await Axios.get('/batches/'+params);
				commit("SET_BATCH" , response.body);
				return true;
			}catch(error){
				return Promise.reject(error);
			}finally{
				commit("SET_LOADING",false);
			}
		},

		// UPDATE BATCHES
		async updateBatches({commit , state},payload){
			try{
				commit("SET_LOADING",true);
				let response = await Axios.put('/batches/'+payload.id , payload);
				commit("SET_BATCHES" , response.body.items);
				return true;
			}catch(error){
				return Promise.reject(error);
			}finally{
				commit("SET_LOADING",false);
			}
		},

		// DELETE BATCHES
		async deleteBatch({commit , state} , params , query){
			if(confirm("Do you really want to delete?")){
				try{
					commit("SET_LOADING" , true);
					let response = await Axios.delete('/batches/' + params + query);
					commit("SET_BATCHES" , response.body.items);
					commit('SET_PAGINATION', response.body.pagination ? response.body.pagination : null);
					return true;
				}catch(error){
					return Promise.reject(error);
				}finally{
					commit("SET_LOADING" , false);
				}
			}
		}
	},

	getters:{

	}
}