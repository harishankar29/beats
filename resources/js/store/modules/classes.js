export default{
	state:{
		loading:false,
		pagination:null,
		classes:[],
		classe:{
			class:'',
			class_status:'',
		}
	},

	mutations:{

		SET_LOADING(state , loading)
		{
			state.loading = loading;
		},

		SET_CLASSE(state , classe)
		{
			state.classe = classe;
		},

		SET_CLASSES(state , classes)
		{
			state.classes = classes;
		},

		SET_PAGINATION(state , pagination){
			state.pagination = pagination;
		}

	},

	actions:{
		// SAVE CLASS
		async saveClass({commit , state},payload){
			try{
				commit("SET_LOADING",true);
				let response = await Axios.post('/classes',payload);
			}catch(error){
				return Promise.reject(error);
			}finally{
				commit("SET_LOADING",false);
			}
		},

		// FETCH CLASS
		async fetchClass({commit , state} , query){
			try{
				commit("SET_LOADING",true);
				let response = await Axios.get('/classes'+ query); 
				commit("SET_CLASSES" , response.body.items);
				commit("SET_PAGINATION" , response.body.pagination ? response.body.pagination : null);
				return true;
			}catch(error){
				return Promise.reject(error);
			}finally{
				commit("SET_LOADING",false);
			}

		},

		// EDIT CLASS
		async editClasse({commit , state},params){
			try{
				commit("SET_LOADING",true);
				let response = await Axios.get('/classes/'+params);
				commit("SET_CLASSE", response.body);
				return true;
			}catch(error){
				return Promise.reject(error);
			}finally{
				commit("SET_LOADING",false);
			}
		},

		// UPDATE CLASS
		async updateClass({commit , state},payload){
			try{
				commit("SET_LOADING",true);
				let response = await Axios.put('/classes/'+ payload.id , payload);
				commit("SET_CLASSES",response.body.items);
				return true;
			}catch(error){
				return Promise.reject(error);
			}finally{
				commit("SET_LOADING",false);
			}
		},
 
		// DELETE CLASS
		async deleteClass({commit , state},params , query){
			if(confirm("Do you really want to delete?")){
				try{
					commit("SET_LOADING",true);
					let response = await Axios.delete('/classes/'+params + query);
					commit("SET_CLASSES" , response.body.items);
					commit("SET_PAGINATION" , response.body.pagination ? response.body.pagination : null);
					return true;
				}catch(error){
					return Promise.reject(error);
				}finally{
					commit("SET_LOADING",false);
				}
			}
		}
	},

	getters:{

	}
}