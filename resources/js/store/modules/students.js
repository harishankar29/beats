export default {
	state:{
		loading : true,
		pagination: null,
		students: [],
		student:{
			name : '',
			class: '',
			school : '',
			profile_pic: '',
			batch : '',
			address : '',
			father: '',
			mother: '',
			father_contact:'',
		}
	},

	mutations:{

		SET_LOADING(state , loading){
			state.loading = loading;
		},

		SET_STUDENT(state , student){
			state.student = student;
		},

		SET_STUDENTS(state , students){
			state.students = students;
		},

		SET_PAGINATION(state, pagination){
			state.pagination = pagination;
		}
	},

	actions:{

		// SAVE STUDENT
		async saveStudent({commit , state},payload){
			try{
				commit("SET_LOADING" , true);
				let response = await Axios.post('/students',payload);

			}catch(error){
				return Promise.reject(error);
			}finally{
				commit("SET_LOADING" , false);
			}
		},

		// EDIT STUDENT
		async editStudent({commit , state} , params){
			try{
				commit("SET_LOADING" , true);
				let response = await Axios.get('/students/'+ params);
				commit("SET_STUDENT" , response.body);
				return true;
			}catch(error){
				return Promise.reject(error);
			}finally{
				commit("SET_LOADING" , false);
			}
		},

		// UPDATE STUDENT
		async updateStudent({commit , state} , payload ){
			try{
				commit("SET_LOADING" , true);
				let response =  await Axios.put('/students/'+ payload.id , payload);
				commit("SET_STUDENTS" , response.body.items);
				return true;
			}catch(error){
				return Promise.reject(error);
			}finally{
				commit("SET_LOADING" , false);
			}
		},

		// FETCHLIST
		async fetchList({commit , state}, query){
			try{
				commit("SET_LOADING" , true);
				let response = await Axios.get('/students' + query);
				commit("SET_STUDENTS" , response.body.items);
				commit('SET_PAGINATION', response.body.pagination ? response.body.pagination : null);
				return true;
			}catch(error){
				return Promise.reject(error);
			}finally{
				commit("SET_LOADING" , false);
			}
		},

		// DELETE  STUDENT 
		async deleteStudent({commit , state} , params , query){
			if(confirm("Do you really want to delete?")){
				try{
					commit("SET_LOADING" , true);
					let response = await Axios.delete('/students/'+ params + query);
					commit("SET_STUDENTS" , response.body.items);
					commit('SET_PAGINATION', response.body.pagination ? response.body.pagination : null);
					return true;
				}catch(error){
					return Promise.reject(error);
				}finally{
					commit("SET_LOADING" , false);
				}
			}
		}
	},

	getters:{

	}
}