export default {

	state :{
		loading:false,
		pagination:null,
		announcements : [],
		announcement  : {
			announcement : '',
			image		 : '',
			batch		 : [],
			
			attach 		 : ''
		}
	},

	mutations:{

		SET_ANNOUNCEMENTS(state, announcements){
			state.announcements = announcements;
		},

		SET_LOADING(state , loading){
			state.loading = loading;
		},

		SET_LISTS(state , announcements){
			state.announcements = announcements;
		} ,

		SET_ANNOUNCEMENT(state, announcement){
			state.announcement = announcement;
		},

		SET_PAGINATION(state , pagination){
			state.pagination = pagination;
		}
	},

	actions : {

		// SAVE
		async saveAnnouncement({commit , state},payload){
			
			try{
				commit("SET_LOADING",true);
				let response = await Axios.post('/announcements',payload);
				
				
			}catch(error){
				return Promise.reject(error);
			}finally{
				commit("SET_LOADING",false);
			}
		},

		// UPDATE
		async updateAnnouncement({commit , state},params)
		{
			try{
				commit("SET_LOADING",true);
				let response = await Axios.put('/announcements/'+ params.id , params);
				commit("SET_ANNOUNCEMENTS",response.body.items);
				return true;
			}catch(error){
				return Promise.reject(error);
			}finally{
				commit("SET_LOADING",true);
			}

		},
		// FETCH
		async fetchAnnouncement({commit , state} , query){
			try{
				commit("SET_LOADING",true);
				let response = await Axios.get('/announcements'+ query);
				commit("SET_LISTS" , response.body.items);
				commit ("SET_PAGINATION" , response.body.pagination ? response.body.pagination : null)
				return true;
			}catch(error){
				return Promise.reject(error);
			}finally{
				commit("SET_LOADING",false);
			}
		},

		// EDIT
		async editAnnouncement({commit , state},id){
			try{
				commit("SET_LOADING",true);
				let response = await Axios.get('/announcements/'+id);
				commit("SET_ANNOUNCEMENT",response.body);
				return true;
			}catch(error){
				return Promise.reject(error);
			}finally{
				commit("SET_LOADING",false);
			}
		},

		// DELETE
		async deleteAnnouncement({commit ,state},params , query){
			if(confirm("Do you really want to delete?")){	
				try{
					commit("SET_LOADING",true);
					let response = await Axios.delete('/announcements/'+ params + query );
					commit("SET_LISTS" , response.body.items);
					commit ("SET_PAGINATION" , response.body.pagination ? response.body.pagination : null);
				}catch(error){
					return Promise.reject(error)
				}finally{
					commit("SET_LOADING",false)
				}
			}
		}
	},

	getters :{

	}
}