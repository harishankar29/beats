export default{
	state :{

		loading : false,
		pagination: null,
		faculties : [],
		faculty :{
			faculty_name : '',
			faculty_subject : '',
			faculty_class :''
		}
	},

	mutations :{

		SET_LOADIING(state , loading){
			state.loading = loading;
		},

		SET_FACULTIES(state , faculties){
			state.faculties = faculties;
		},

		SET_FACULTY(state , faculty){
			state.faculty = faculty;
		},

		SET_PAGINATION(state , pagination){
			state.pagination = pagination;
		}
	},

	actions:{
		// SAVE FACULTY
		async saveFaculty({commit , state},payload)
		{
			try{
				commit("SET_LOADIING" , true);
				let response = await Axios.post('/faculties',payload);
				return true;
			}catch(error){
				return Promise.reject(error);
			}finally{
				commit("SET_LOADIING" , false);
			}
		},

		// LIST FACULTY
		async listFaculty({commit , state} , query)
		{
			try{
				commit("SET_LOADIING" , true);
				let response = await Axios.get('/faculties' + query);
				commit("SET_FACULTIES" , response.body.items);
				commit("SET_PAGINATION" , response.body.pagination ? response.body.pagination : null);
				return true;
			}catch(error){
				return Promise.reject(error);
			}finally{
				commit("SET_LOADIING" , false);
			}
		},

		// DELETE FACULTY
		async deleteFaculty({commit , state} , params , query)
		{
			if(confirm("Do you really want to delete?")){
				try{
					commit("SET_LOADIING" , true);
					let response = await Axios.delete('/faculties/'+ params + query);
					commit("SET_FACULTIES" , response.body.items);
					commit("SET_PAGINATION" , response.body.pagination ? response.body.pagination : null);
					return true;
				}catch(error){
					return Promise.reject(error);
				}finally{
					commit("SET_LOADIING" , false);
				}
			}
		},

		// EDIT FACULTY
		async editFaculty({commit , state} , payload){
			try{
				commit("SET_LOADIING", true);
				let response = await Axios.get('/faculties/'+ payload);
				commit("SET_FACULTY" , response.body);
				return true;
			}catch(error){
				return Promise.reject(error);
			}finally{
				commit("SET_LOADIING", false);
			}
		},

		// UPDATE FACULTY
		async updateFaculty({commit , state} ,params){
			try{
				commit("SET_LOADIING" , true);
				let response = await Axios.put('/faculties/'+params.id , params);
				commit("SET_FACULTIES" , response.body.items);
				return true;
			}catch(error){
				return Promise.reject(error);
			}finally{
				commit("SET_LOADIING",false);
			}
		}
	},

	getters:{

	}
}