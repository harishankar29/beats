export default{
	state:{

		loading : false,
		pagination :null,
		assignments:[],
		assignment:{
			assignment : '',
			topic:   '',
			submission_date : '',
			attachement : '',
			classe     : [] ,
			attach_name : '',
			
		}
	},

	mutations:{

		SET_LOADING(state,loading){
			state.loading = loading;
		},

		SET_ASSIGNMENT(state , assignment){
			state.assignment = assignment;
		},

		SET_ASSIGNMENTS(state , assignments){
			state.assignments = assignments;
		},

		SET_PAGINATION(state , pagination){
			state.pagination = pagination;
		}

	},

	actions:{

		// SAVE ASSIGNMENT
		async saveAssignment({commit , state},payload){
			try{
				commit("SET_LOADING",true);
				let response = await Axios.post('/assignments',payload);
				return true;
			}catch(error){
				return Promise.reject(error);
			}finally{
				commit("SET_LOADING",false);
			}
		},

		// FETCH ASSIGNMENT
		async fetchAssignment({commit , state} , query){
			try{
				commit("SET_LOADING",true);
				let response = await Axios.get('/assignments' + query);
				commit("SET_ASSIGNMENTS", response.body.items);
				commit("SET_PAGINATION" , response.body.pagination ? response.body.pagination : null);
				return true;
			}catch(error){
				return Promise.reject(error);
			}finally{
				commit("SET_LOADING",false);
			}
		},

		// EDIT ASSIGNMENT
		async editAssignment({commit , state},params){
			try{
				commit("SET_LOADING",true);
				let response = await Axios.get('/assignments/'+ params);
				commit("SET_ASSIGNMENT", response.body);
				return true;
			}catch(error){
				return Promise.reject(error);	
			}finally{
				commit("SET_LOADING",false);
			}
		},

		// UPDATE ASSIGNMENT
		async updateAssignment({commit , state},payload){
			try{
				commit("SET_LOADING",true);
				let response = await Axios.put('/assignments/'+ payload.id , payload);
				commit("SET_ASSIGNMENTS", response.body.items);
				return true;
			}catch(error){
				return Promise.reject(error);
			}finally{
				commit("SET_LOADING",false);
			}
		},

		// DELETE ASSIGNMENT
		async deleteAssign({commit , state},params ,query){
			if(confirm("Do you really want to delete?")){
				try{
					commit("SET_LOADING",true);
					let response = await Axios.delete('/assignments/'+ params + query);
					commit("SET_ASSIGNMENTS", response.body.items);
					commit ("SET_PAGINATION" , response.body.pagination ? response.body.pagination : null);
					return true;
				}catch(error){
					return Promise.reject(error);
				}finally{
					commit("SET_LOADING",false);
				}
			}
		}
	},

	getters:{

	}
}