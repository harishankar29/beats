import axios from "axios";
import NProgress from 'nprogress'
import 'nprogress/nprogress.css'

axios.defaults.withCredentials = true;
axios.defaults.baseURL = APIURL;
axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
axios.defaults.headers.common['X-CSRF-TOKEN'] = CSRF_TOKEN



axios.interceptors.request.use(function (config) {
    NProgress.start()
    return config;
  }, function (error) {
    NProgress.done()
    return Promise.reject(error);
  }); 

  
axios.interceptors.response.use(function (response) {
    NProgress.done()
    return response.data;
  }, function (error) {
  	NProgress.done()
  	 
    toast.fire(
            'Failed',
            error.response.data.message,
            'error'
        );
    return Promise.reject(error.response.data);
  });
	
window.Axios = axios;	 