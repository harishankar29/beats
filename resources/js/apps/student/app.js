/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('../../bootstrap');

window.Vue = require('vue');


import Vue from 'vue';
import router from './router.js';
import store from '../../store/index'
import Paginate from 'vuejs-paginate'

Vue.component('paginate', Paginate);
const app = new Vue({
    el: '#app',
	router,
	store    
});

export default app;