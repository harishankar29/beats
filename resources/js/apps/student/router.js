import Vue from 'vue'
import VueRouter from 'vue-router'
import StudentView from './components/Student.vue';

Vue.use(VueRouter)

const routes = [
		
		{
			path :'/home',
			component : StudentView
		},

];

const router = new VueRouter({
	routes,
	mode: 'history',
    
});

export default router;