import Vue from 'vue'
import VueRouter from 'vue-router'
import NProgress from 'nprogress'
import 'nprogress/nprogress.css'

/*Dashboard Component*/
import AdminView from './components/Admin.vue';

/*Announcement Component*/
import AnnouncementCreate from './components/Announcement/Create';
import AnnouncementEdit from './components/Announcement/Edit';
import AnnouncementList   from './components/Announcement/List';
import AnnouncementView   from './components/Announcement/View';

/*Assignment Component*/
import AssignmentCreate from './components/Assignment/Create';
import AssignmentEdit from './components/Assignment/Edit';
import AssignmentList   from './components/Assignment/List';
import AssignmentView   from './components/Assignment/View';

/*Batch Component*/
import BatchCreate from './components/Batch/Create';
import BatchEdit from './components/Batch/Edit';
import BatchList   from './components/Batch/List';
import BatchView   from './components/Batch/View';

/*Class Component*/
import ClassCreate from './components/Class/Create';
import ClassEdit from './components/Class/Edit';
import ClassList   from './components/Class/List';
import ClassView   from './components/Class/View';

/*Faculty Component*/
import FacultyCreate from './components/Faculty/Create';
import FacultyEdit   from './components/Faculty/Edit';
import FacultyList   from './components/Faculty/List';
import FacultyView   from './components/Faculty/View';

/*Static Page*/
import StaticCreate from './components/StaticPage/Create';
import StaticList   from './components/StaticPage/List';
import StaticView   from './components/StaticPage/View';

/*Student Component*/
import StudentCreate from './components/Student/Create';
import StudentEdit   from './components/Student/Edit';
import StudentList   from './components/Student/List';
import StudentView   from './components/Student/View';

/*Subject Component*/
import SubjectCreate from './components/Subject/Create';
import SubjectEdit   from './components/Subject/Edit';
import SubjectList   from './components/Subject/List';
import SubjectView   from './components/Subject/View';

/* Account Create */
import AccountStudent from './components/Account/CreateStudent';
import AccountFaculty from './components/Account/CreateFaculty';
import AccountList from './components/Account/List';


/*Profile Component*/
import Profile from './components/Profile/Create';

Vue.use(VueRouter)

const routes = [
		
		{
			path :'/',
			component : AdminView
		},

		/*Announcement Routes*/
		{
			path : '/announcements',
			component : AdminView,
			children:[
				{
					path: 'create',
					component: AnnouncementCreate
				},

				{
					path : 'edit/:id',
					component:AnnouncementEdit,
					name: 'EditAnnouncement',
					props: true
				},

				{
					path : 'list',
					component :AnnouncementList
				}
			]
		},

		/*Assignment Routes*/

		{
			path: '/assignments',
			component : AdminView,
			children : [
				{
					path : 'create',
					component : AssignmentCreate
				},

				{
					path : 'edit/:id',
					component : AssignmentEdit,
					name : 'EditAssignment',
					props : true
				},

				{
					path : 'list',
					component : AssignmentList
				}
			]
		},



		/*Account Routes*/

		{
			path: '/account',
			component : AdminView,
			children : [
				{
					path : 'create/student',
					component : AccountStudent
				},

				{
					path : 'create/faculty',
					component : AccountFaculty
				},

				/* {
					path : 'edit/:id',
					component : AccountEdit,
					name : 'EditAccount',
					props : true
				}, */

				{
					path : 'list',
					component : AccountList
				}
			]
		},



		/*Batch Routes*/

		{
			path:'/batches',
			component : AdminView,
			children:[
				{
					path : 'create',
					component : BatchCreate
				},

				{
					path : 'edit/:id',
					component : BatchEdit,
					name : 'EditBatches',
					props : true,
				},

				{
					path : 'list',
					component : BatchList
				}

			]
		},

		/*Class Route*/
		{
			path : '/class',
			component : AdminView,
			children : [
				{
					path : 'create',
					component : ClassCreate
				},

				{
					path : 'edit/:id',
					component : ClassEdit,
					name : 'EditClass',
					props: true
				},

				{
					path : 'list',
					component : ClassList
				}
			]
		},

		/*Faculty Route*/

		{
			path : '/faculty',
			component : AdminView,
			children : [
				{
					path : 'create',
					component : FacultyCreate
				},

				{
					path : 'edit/:id',
					component : FacultyEdit,
					name : 'EditFaculty',
					props : true
				},

				{
					path : 'list',
					component : FacultyList
				}
			]
		},

		/*Static Route*/
		{
			path : '/static',
			component : AdminView,
			children : [
				{
					path : 'create',
					component: StaticCreate
				},
				{
					path : 'list',
					component : StaticList
				}
			]
		},

		/*Student Route*/
		{
			path : '/students',
			component : AdminView,
			children : [
				{
					path : 'create',
					component : StudentCreate
				},

				{
					path : 'edit/:id',
					component : StudentEdit,
					name : 'EditStudent',
					props:true
				},

				{
					path: 'list',
					component : StudentList
				}
			]
		},

		/*Subject Route*/
		{
			path : '/subjects',
			component : AdminView,
			children :[
				{
					path : 'create',
					component : SubjectCreate
				},

				{
					path : 'edit/:id',
					component : SubjectEdit,
					name :  'EditSubject',
					props : true
				},

				{
					path : 'list',
					component : SubjectList
				}
			]
		},

		/*Profile Route */
		{
			path:'/profile',
			component:Profile
		}
];

const router = new VueRouter({
	routes,
	linkActiveClass : 'active',
    linkExactActiveClass:'exact-active'
});

router.beforeResolve((to, from, next) => {
    
        NProgress.start()
    
    next()
});

router.afterEach((to, from) => {
    NProgress.done()
});
export default router;