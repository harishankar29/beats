<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="url" content="{{config('app.url')}}">
    <meta name="api_url" content="{{config('app.api_url')}}">
    
    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    
    <!-- Styles -->
    <link href="{{asset('css/student.css')}}" rel="stylesheet">
    
</head>
<body>
    
        <div class="header py-4">
                <div class="container">
                <div class="d-flex">
                    <a class="header-brand" href="/home">
                    <img src="images/1.png" class="header-brand-img" alt="Beats Academy">
                    </a>
                    <div class="d-flex order-lg-2 ml-auto">
                    <div class="dropdown d-none d-md-flex">
                        <a class="nav-link icon" data-toggle="dropdown">
                            <div class="container">
                                <div class="row">
                                    <div class="col-sm-3">
                                        <i class="fe fe-home px-3"></i>
                                    </div>
                                </div>      
                                <div class="row">
                                    <div class="col-sm-3">
                                        <p>Home</p>
                                    </div>
                                </div>
                                
                            </div>
                        </a>
                        <a class="nav-link icon" data-toggle="dropdown">
                            <div class="container">
                                <div class="row">
                                    <div class="col-sm-3 ">
                                        <i class="fe fe-compass px-4"></i>
                                    </div>
                                </div>      
                                <div class="row">
                                    <div class="col-sm-3">
                                        <p>Discover</p>
                                    </div>
                                </div>
                        </div>
                        </a>
                        <a class="nav-link icon" data-toggle="dropdown">
                        <div class="container">
                                <div class="row">
                                    <div class="col-sm-3 ">
                                        <i class="fe fe-bell px-5"></i>
                                    </div>
                                </div>      
                                <div class="row">
                                    <div class="col-sm-3">
                                        <p>Notification</p>
                                    </div>
                                </div>
                        </div>
                        </a>
                    </div>
                    <div class="dropdown">
                        <a href="#" class="nav-link pr-0 leading-none" data-toggle="dropdown">
                        <span class="avatar" style="background-image: url(img/faces/female/25.jpg)"></span>
                        <span class="ml-2 d-none d-lg-block">
                            <span class="text-default">{{ Auth::user()->name }}</span>
                            <small class="text-muted d-block mt-1">{{ Auth::user()->role }}</small>
                        </span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                        
                        <a class="dropdown-item" href="{{ route('logout') }}"
                            onclick="event.preventDefault();
                                            document.getElementById('logout-form').submit();">
                            <i class=" dropdown-icon  fe fe-log-out"></i>{{ __('Logout') }}
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                        </a>
                        </div> 
                    </div>
                    </div>
                    <a href="#" class="header-toggler d-lg-none ml-3 ml-lg-0 collapsed" data-toggle="collapse" data-target="#headerMenuCollapse" aria-expanded="false">
                    <span class="header-toggler-icon"></span>
                    </a>
                </div>
                </div>
            </div>

        <div id="app">
            @yield('content')
        </div>
    
    <script src="/js/student.js"></script>
    <script src="/jq/jquery-3.2.1.min.js"></script>
    {{-- <script src="/js/plugins/tabler/js/require.min.js"></script> --}}
    {{-- <script src="/js/plugins/tabler/js/dashboard.js"></script> --}}

</body>
</html>
