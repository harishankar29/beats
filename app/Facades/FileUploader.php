<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class FileUploader extends Facade
{
    public static function getFacadeAccessor()
    {
        return 'fileUploader';
    }
}
