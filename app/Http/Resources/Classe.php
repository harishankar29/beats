<?php

namespace App\Http\Resources;

class Classe extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
       
        return [
            
            "id"           => $this->id,
            "class"        => $this->class,
            "class_status" => $this->class_status,

        ];
    }
}
