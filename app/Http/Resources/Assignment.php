<?php

namespace App\Http\Resources;

class Assignment extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id"              => $this->id,
            "assignment"      => $this->assignment,
            "topic"           => $this->topic,
            "submission_date" => $this->submission_date,
            "attachement"     => $this->attachement,
            "attach_name"     => $this->attach_name

        ];
    }
}
