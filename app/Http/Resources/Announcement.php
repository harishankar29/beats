<?php

namespace App\Http\Resources;

  

class Announcement extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [ 
           
            "id"           => $this->id,
            "announcement" => $this->announcement,
            "image"        => $this->image,
            "batch"        => $this->batch,
            "class"        => $this->class,
            "attach"       => $this->attach,
            "created_at"   => $this->created_at,
            
            // "owner"        => new User($this->whenLoaded('owner')),
            // "classes"      => new Classe($this->whenLoaded('classes'))
            
        ];
    }
}
