<?php

namespace App\Http\Resources;
use Auth;
class Student extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            "id"             => $this->id,
            "name"           => $this->name,
            "class"          => $this->class,
            "school"         => $this->school,
            "profile_pic"    => $this->profile_pic,
            "batch"          => $this->batch,
            "address"        => $this->address,
            "father"         => $this->father,
            "mother"         => $this->mother,
            "father_contact" => $this->father_contact,
            "created_by"     => Auth::user()->name,
            "updated_by"     => $this->updated_by,
            "deleted_by"     => $this->deleted_by,

        ];
    }
}
