<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection as JsonCollection;

class PaginatorCollection extends JsonCollection
{
    /**
     * Pagination holder
     */
    protected $pagination;

    /**
     * Overwrite Resource Collection construct
     */
    public function __construct($resource)
    {
        $this->generatePagination($resource);

        $resource = $resource->getCollection();

        parent::__construct($resource);
    }

    /**
     * Convert to array resource
     */
    public function toArray($request)
    {   
        $data = [
            'items' => $this->collection,
        ];

        if ($this->pagination['totalPages'] > 1) {
            $data['pagination'] = $this->pagination;
        }

        return $data;
    }

    /**
     * Generate custom paginator collection
     */
    protected function generatePagination($resource)
    {
        $this->pagination = [
            'total' => $resource->total(),
            'count' => $resource->count(),
            'perPage' => $resource->perPage(),
            'currentPage' => $resource->currentPage(),
            'totalPages' => $resource->lastPage(),
            'first' => $resource->url(1),
            'last' => $resource->url($resource->lastPage()),
            'next' => $resource->nextPageUrl(),
            'prev' => $resource->previousPageUrl()
        ];
    }
}
