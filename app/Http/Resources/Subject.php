<?php

namespace App\Http\Resources;

class Subject extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [

            "id"            => $this->id,
            "class"         => $this->class,
            "subject"        => $this->subject,
            "faculty"        => $this->faculty,
            
        ];
    }
}
