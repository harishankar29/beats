<?php

namespace App\Http\Resources;

class Batch extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [ 
            'id'         => $this->id,
            'batch'      => $this->batch,
            'class'      => $this->class,
            'subject'    => $this->subject,
            'center'     => $this->center,
            'date'       => $this->date,
            'start_time' => $this->start_time,
            'end_time'   => $this->end_time,
            'weekday'    => $this->weekday,

        ];
    }
}
