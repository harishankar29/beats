<?php

namespace App\Http\Services;

class ApiResponse
{
	/*Api response message holder*/

	protected $message = '';


	public function setMessage($message)
	{
		$this->message = $message;

		return $this;
	}

	/*Output Api response*/

	public function output($output = null ,$statusCode = 200)
	{
		$payload = ['status'=>$statusCode];

		($output !=null) ? $payload['body'] = $output : '';

		($this->message !=null) ? $payload['message'] = $this->message : '';

		return response($payload)->setStatusCode($statusCode);
	}

	public function noData($statusCode = 200)
	{
		return $this->output(null,$statusCode);
	}
}