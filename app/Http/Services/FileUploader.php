<?php

namespace App\Http\Services;

use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

/**
 * File uploader class as a wrapper to lavel storage
 */
class FileUploader
{
    private const ACCESS_PUBLIC = 'public';
    private const ACCESS_PRIVATE = 'private';

    /**
     * Allowed mime types
     *
     * @var array
     */
    private $mimeTypes = ['image/jpeg', 'image/jpg', 'image/png', 'image/gif'];

    /**
     * Uploaded file mime type
     *
     * @var string
     */
    private $mimeType = null;

    /**
     * File uploader key
     *
     * @var string
     */
    private $fileKey = 'file';

    /**
     * File name to be upload
     *
     * @var string
     */
    private $fileName = null;

    /**
     * File access 'public' or 'private'
     *
     * @var string
     */
    private $access = self::ACCESS_PUBLIC;

    /**
     * File size in KB
     *
     * @var integer
     */
    private $size = 1000;

    /**
     * Set path respective to public and private folders
     *
     * @var string
     */
    private $path = '';

    /**
     * Is file required
     *
     * @var boolean
     */
    private $required = false;

    /**
     * Throw validation error
     *
     * @var boolean
     */
    private $throwError = true;

    /**
     * Error holder
     *
     * @var string
     */
    private $error = null;

    /**
     * Set allowed mime types for upload file
     *
     * @param array $mimeTypes
     * @return self
     */
    public function setMimeTypes(array $mimeTypes)
    {
        $this->mimeTypes = $mimeTypes;

        return $this;
    }

    /**
     * Set uploader file key
     *
     * @param string $fileKey
     * @return self
     */
    public function setFileKey(string $fileKey)
    {
        $this->fileKey = $fileKey;

        return $this;
    }

    /**
     * File name to be upload
     *
     * @param string $fileName
     * @return self
     */
    public function setFileName(string $fileName)
    {
        $this->fileName = $fileName;

        return $this;
    }

    /**
     * Set max file size in KB (should not be greater than 2000 KB)
     *
     * @param integer $size 
     * @return void
     */
    public function setSize(int $size)
    {
        $this->access = ($size <= 2000) ? $size : $this->size;

        return $this;
    }

    /**
     * Set file upload path
     *
     * @param string $path
     * @return self
     */
    public function setPath(string $path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * Set file as private
     *
     * @return boolean
     */
    public function isPrivate()
    {
        $this->access = self::ACCESS_PRIVATE;

        return $this;
    }

    /**
     * File required
     *
     * @param boolean $required
     * @return boolean
     */
    public function isRequired($required = false)
    {
        $this->required = $required;

        return $this;
    }

    /**
     * Set throw error true, false
     *
     * @param bool $throwError
     * @return void
     */
    public function throwError($throwError = true)
    {
        $this->throwError = $throwError;

        return $this;
    }

    /**
     * Get uploaded file mime type
     *
     * @return string
     */
    public function getMimeType()
    {
        return $this->mimeType;
    }

    /**
     * Get error
     *
     * @return string
     */
    public function getError()
    {
        return $this->error;
    }

    /**
     * Upload file 
     *
     * @return string | null
     */
    public function upload(UploadedFile $file = null)
    {
        $file = ($file) ?? request()->file($this->fileKey);

        if ($file && $this->validate($file)) {
            $this->mimeType = $file->getMimeType();

            $storage = Storage::disk($this->access);

            $filePath =  ($this->fileName)
                ? $storage->putFileAs($this->path, $file, $this->fileName . '.' . $file->clientExtension())
                : $storage->putFile($this->path, $file);

            return $this->access == self::ACCESS_PRIVATE ? ('files/' . $filePath) : ('/storage/' . $filePath);
        }

        return null;
    }

    /**
     * Upload multiple files
     *
     * @return void
     */
    public function multipleUpload()
    {
        $data = [];

        foreach (request()->file($this->fileKey) as $file) {
            $data[] = $this->upload($file);
        }

        return $data;
    }


    /**
     * Delete files from storage physically
     *
     * @param string $path
     * @return void
     */
    public function delete($path)
    {
        $path = str_replace('storage', 'public', $path);

        if (Storage::exists($path)) {
            Storage::delete($path);
        }
    }

    /**
     * Move file from one place to another
     *
     * @param [type] $path
     * @return void
     */
    public function move($path)
    {
        $path = str_replace('storage', 'public', $path);

        if (Storage::exists($path)) {

            if (strpos($path, 'public') !== false) { //move to public 
                $newPath = str_replace('public', 'files', $path);
            } else {
                $newPath = str_replace('files', 'public', $path);
            }

            Storage::move($path, $newPath);

            return $newPath;
        }

        return null;
    }

    /**
     * Validate upload field and throw error
     *
     * @return boolean
     */
    protected function validate($file = null)
    {
        $postData = ($file) ? [$this->fileKey => $file] : request()->all();

        $validator = Validator::make($postData, $this->validationRules());

        if ($this->throwError && $validator->fails()) {
            abort(422, $validator->errors()->first());
        } else if ($validator->fails()) {
            $this->error = $validator->errors()->first();

            return false;
        }

        return true;
    }

    /**
     * Validation rule for file upload
     *
     * @return void
     */
    private function validationRules()
    {
        $required = $this->required ? 'required' : 'sometimes';

        $mimeTypes = 'mimetypes:' . implode(',', $this->mimeTypes);

        $size = 'max:' . $this->size;

        return [
            $this->fileKey => [$required, 'file', $mimeTypes, $size]
        ];
    }
}
