<?php
 
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Repositories\AssignmentRepository as AssignmentRepository;
use App\Http\Requests\Assignment as AssignmentRequest;
use App\Http\Resources\Assignment as AssignmentResource;
use App\Http\Resources\AssignmentCollection as AssignmentCollection;
use App\Models\Assignment;
use Auth;

class AssignmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(AssignmentRepository $assignmentRepository)
    {
        $list = (Auth::user()->role == 'admin') 
                ? $assignmentRepository->list()['paginator']
                : $assignmentRepository->student();
        
        return $this->response->output(new AssignmentCollection($list));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AssignmentRequest $request)
    {
        
        $newAssignment = $request->save();

        return $this->response->output(new AssignmentResource($newAssignment));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show( Assignment $assignment)
    {
        return $this->response->output(new AssignmentResource($assignment));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AssignmentRequest $request, Assignment $assignment)
    {

        $newAssignment = $request->save($assignment);

        return $this->response->output(new AssignmentResource($newAssignment));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy( AssignmentRepository $assignmentRepository , Assignment $assignment)
    {
        $assignmentData = $assignment->delete();

        $list = ($assignmentData)
            ? $assignmentRepository->list()['paginator']
            : $this->response->noData();

        return $this->response->output(new AssignmentCollection($list));
    }
}
