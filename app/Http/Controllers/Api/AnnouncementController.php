<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Announcement as AnnouncementRequest;
use App\Http\Resources\Announcement as AnnouncementResource;
use App\Http\Resources\AnnouncementCollection as AnnouncementCollection;
use App\Models\Repositories\AnnouncementRepository as  AnnouncementRepository;
use App\Models\Announcement;
use App\Models\Announce_Batch ;
use Auth;

class AnnouncementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(AnnouncementRepository $AnnouncementRepository)
    {   
        
        $listAnnouncement =  (Auth::user()->role == 'admin') 
                ? $AnnouncementRepository->list()['paginator']
                : $AnnouncementRepository->student();
                
            return $this->response->output(new AnnouncementCollection($listAnnouncement));
    }

    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AnnouncementRequest $request )
    {   
        
        $newAnnouncement = $request->save();
        
        return $this->response->output(new AnnouncementResource($newAnnouncement));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Announcement $announcement)
    {
        return $this->response->output(new AnnouncementResource($announcement));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AnnouncementRequest $request , Announcement $announcement)
    {
        $announcement = $request->save($announcement);

        return $this->response->output(new AnnouncementResource($announcement));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(AnnouncementRepository $AnnouncementRepository , Announcement $announcement)
    {
        
        $newList = $announcement->delete();

        $list = ($newList)
                    ? $AnnouncementRepository->list()['paginator']
                    : $this->response->noData();
     
                return $this->response->output(new AnnouncementCollection($list));
    }
}
