<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Repositories\StudentRepository as StudentRepository;
use App\Http\Requests\Student as StudentRequest;
use App\Http\Resources\Student as StudentResource;
use App\Http\Resources\StudentCollection as StudentCollection;
use App\Models\Student;
use Auth;
 
class StudentController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(StudentRepository $studentRepository)
    {
        $list = (Auth::user()->role == 'admin') 
                ? $studentRepository->list()['paginator']
                : $studentRepository->student();
        
        return $this->response->output(new StudentCollection($list));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StudentRequest $request)
    {
        // dd($request);
        $newStudent = $request->save();

        return $this->response->output(new StudentResource($newStudent));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Student $student)
    {
        return $this->response->output(new StudentResource($student));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StudentRequest $request, Student $student)
    {
        $newStudent = $request->save($student);

        return $this->response->output(new StudentResource($newStudent));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(StudentRepository $studentRepository , Student $student)
    {
        $studentBlog = $student->delete();

        $list = ($studentBlog)
                    ? $studentRepository->list()['paginator']
                    : $this->response->noData();
     
                return $this->response->output(new StudentCollection($list));
    }
}
