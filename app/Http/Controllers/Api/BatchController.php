<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Batch as BatchRequest;
use App\Http\Resources\Batch as BatchResource;
use App\Http\Resources\BatchCollection as BatchCollection;
use App\Models\Repositories\BatchRepository as  BatchRepository;
use App\Models\Batch;

class BatchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(BatchRepository $batchRepository)
    {
        $list = $batchRepository->list()['paginator'];

        return $this->response->output(new BatchCollection($list));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BatchRequest $request)
    {
        $newBatch = $request->save();

        return $this->response->output(new BatchResource($newBatch));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show( Batch $batch)
    {
        return $this->response->output(new BatchResource($batch));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(BatchRequest $request, Batch $batch)
    {
        $newBatch = $request->save($batch);

        return $this->response->output(new BatchResource($newBatch));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(BatchRepository $batchRepository ,  Batch $batch)
    {
        $deleteList = $batch->delete();

        $list = ($deleteList)
                    ? $batchRepository->list()['paginator']
                    : $this->response->noData();
     
                return $this->response->output(new BatchCollection($list));
    }
}
