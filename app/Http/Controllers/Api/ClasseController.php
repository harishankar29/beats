<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Repositories\ClasseRepository as ClasseRepository;
use App\Http\Requests\Classe as ClasseRequest;
use App\Http\Resources\Classe as ClasseResource;
use App\Http\Resources\ClasseCollection as ClasseCollection;
use App\Models\Classe;

class ClasseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(ClasseRepository $classeRepository)
    {
        $list = $classeRepository->list()['paginator'];
        
        return $this->response->output(new ClasseCollection($list));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ClasseRequest $request)
    {
        $newClasse = $request->save();

        return $this->response->output(new ClasseResource($newClasse));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show( Classe $classe)
    {
        
        return $this->response->output(new ClasseResource($classe));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ClasseRequest $request, Classe $classe)
    {
        $newClasse = $request->save($classe);

        return $this->response->output(new ClasseResource($newClasse));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(ClasseRepository $classeRepository ,  Classe $classe)
    {
       $listClass =  $classe->delete();

       $list = ($listClass)
                    ? $classeRepository->list()['paginator']
                    : $this->response->noData();
     
        return $this->response->output(new ClasseCollection($list));
    }
}
