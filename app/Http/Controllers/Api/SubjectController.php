<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Repositories\SubjectRepository as SubjectRepository;
use App\Http\Requests\Subject as SubjectRequest;
use App\Http\Resources\Subject as SubjectResource;
use App\Http\Resources\SubjectCollection as SubjectCollection;
use App\Models\Subject;
use Auth;

class SubjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(SubjectRepository $subjectRepository)
    {
        $list = (Auth::user()->role == 'admin') 
                ? $subjectRepository->list()['paginator']
                : $subjectRepository->student();
        
        return $this->response->output(new SubjectCollection($list));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SubjectRequest $request)
    {
        $newSubject = $request->save();

        return $this->response->output(new SubjectResource($newSubject));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Subject $subject)
    {
        return $this->response->output(new SubjectResource($subject));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(SubjectRequest $request, Subject $subject)
    {
        $newSubject = $request->save($subject);

        return $this->response->output(new SubjectResource($newSubject));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Subject $subject , SubjectRepository $subjectRepository)
    {
        $subjectList = $subject->delete();

        $list = ($subjectList)
        ? $subjectRepository->list()['paginator']
        : $this->response->noData();

        return $this->response->output(new SubjectCollection($list));
    }
}
