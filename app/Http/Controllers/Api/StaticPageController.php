<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\StaticPage as StaticPageRequest;
use App\Http\Resources\StaticPage as StaticPageResource;
use App\Http\Resources\StaticPageCollection as StaticPageCollection;
use App\Models\StaticPage;

class StaticPageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StaticPageRequest $request)
    {
        $newPage = $request->save();

        return $this->response->output(new StaticPageResource($newPage));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show( StaticPage $static)
    {
        return $this->response->output(new StaticPageResource($static));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StaticPageRequest $request, StaticPage $static)
    {
        $newPage = $request->save($static);

        return $this->response->output(new StaticPageResource($newPage));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy( StaticPage $static)
    {
        $static->delete();
    }
}
