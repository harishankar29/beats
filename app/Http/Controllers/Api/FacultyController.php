<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Repositories\FacultyRepository as FacultyRepository;
use App\Http\Requests\Faculty as FacultyRequest;
use App\Http\Resources\Faculty as FacultyResource;
use App\Http\Resources\FacultyCollection as FacultyCollection;
use App\Models\Faculty;
use Auth;

class FacultyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(FacultyRepository $facultyRepository)
    {
        $list = (Auth::user()->role == 'admin') 
                ? $facultyRepository->list()['paginator']
                : $facultyRepository->student();
        
        return $this->response->output(new FacultyCollection($list)); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(FacultyRequest $request)
    {
        $newFaculty = $request->save();

        return $this->response->output(new FacultyResource($newFaculty));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Faculty $faculty)
    {
        return $this->response->output(new FacultyResource($faculty));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(FacultyRequest $request, Faculty $faculty)
    {
        $newFaculty = $request->save($faculty);

        return $this->response->output(new FacultyResource($newFaculty));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(FacultyRepository $facultyRepository , Faculty $faculty)
    {
        $listFaculty = $faculty->delete();

        $list = ($listFaculty)
            ? $facultyRepository->list()['paginator']
            : $this->response->noData();

        return $this->response->output(new FacultyCollection($list));
    }
}
