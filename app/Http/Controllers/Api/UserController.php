<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Repositories\UserRepository as UserRepository;
use App\Http\Requests\User as UserRequest;
use App\Http\Resources\User as UserResource;
use App\Http\Resources\UserCollection as UserCollection;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $list = User::latest()->paginate(10);
        // dd($list);
        return $this->response->output(new UserCollection($list));    
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       
        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->class = $request->class;
        $request->type  ? $user->role = $request->type : $user->role = $request->type1; 
        
        $user->save();

 
        return response()->json($user,200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserRequest $request, User $user)
    {
        $details = $request->save($user);

        return $this->response->output(new UserResource($details));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $newList = $user->delete();
        $list = ($newList)
        ? User::latest()->paginate(10)
        : $this->response->noData();

       
        return $this->response->output(new UserCollection($list));
    }
}
