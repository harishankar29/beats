<?php

namespace App\Http\Controllers;

use App\Http\Services\ApiResponse;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /*
    * Api Response Holder
    */
    protected $response = null;

    /**
		Auth User Holder
    */
	protected $authUser = null;

	public function __construct()
	{
		$this->response = new ApiResponse;
	}
}
