<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth' , 'verified']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $layouts = (Auth::user()->role == 'admin' || Auth::user()->role == 'Faculty') ? 'layouts.admin' : 'layouts.student';
        return view('home',compact('layouts'));
    }
}
