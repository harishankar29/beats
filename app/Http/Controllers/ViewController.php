<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Student;
class ViewController extends Controller
{

	public function home(){
		return view('front/master');
	}

    public function about()
    {
    	return view('front/about-us');
    }

    public function contact(){
    	return view('front/contact');
    }

    public function detail(){
    	return view('front/course-details');
    }

    public function courses(){
    	return view('front/courses');
    }

    
}
