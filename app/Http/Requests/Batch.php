<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\Repositories\BatchRepository;

class Batch extends FormRequest
{   

    public function __construct(BatchRepository $batchRepository)
    {
        $this->batchRepository = $batchRepository;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "batch"        => "required",
            "class"        => "required",
            "subject"      => "required",
            "center"       => "required",
            "date"         => "required",
            "start_time"   => "required",
            "end_time"     => "required",
            "weekday"      => "required",
            
        ];
    }

    public function messages(){
        return[ 
            "batch.required"      => "Batch Name is required",
            "class.required"      => "Class is required",
            "subject.required"    => "Subject is required",
            "center.required"     => "Center is required",
            "date.required"       => "Date is required",
            "start_time.required" => "StartTime is required",
            "end_time.required"   => "End Time is required",
            "weekday.required"    => "Weekday is required"
         ];
    }

    public function save($batch = null)
    {
        $data = $this->validated();

        return($batch)
            ? $this->batchRepository->update($data , $batch->id)
            : $this->batchRepository->create($data);
    }       
}
