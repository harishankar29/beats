<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\Repositories\ClasseRepository;

class Classe extends FormRequest
{   

    public function __construct(ClasseRepository $classeRepository)
    {
        $this->classeRepository = $classeRepository;
    }
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "class"        => "required",
            "class_status" => "required"
        ];
    }

    public function messages(){
        return [
            "class.required"        => "Class Name is required",
            "class_status.required" => "Class Status is required"
        ];
    }

    public function save($classe = null)
    {
        $data = $this->validated();

        

        return($classe)
            ? $this->classeRepository->update($data , $classe->id)
            : $this->classeRepository->create($data);
    }
}
