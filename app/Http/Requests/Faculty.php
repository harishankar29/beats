<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\Repositories\FacultyRepository;

class Faculty extends FormRequest
{

    public function __construct(FacultyRepository $facultyRepository)
    {
        $this->facultyRepository = $facultyRepository;
    }
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "faculty_name"     => "required",
            "faculty_subject"  => "required",
            "faculty_class"    => "required",
        ];
    }

    public function messages(){
        return [
            "faculty_name.required"    => "Faculty Name is required",
            "faculty_subject.required" => "Faculty Subject is required",
            "faculty_class.required"   => "Faculty Class is required"
        ];
    }

    public function save($faculty = null)
    {
        $data = $this->validated();

        return($faculty)
            ? $this->facultyRepository->update($data,$faculty->id)
            : $this->facultyRepository->create($data);
    }
}
