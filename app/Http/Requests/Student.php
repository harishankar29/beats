<?php

namespace App\Http\Requests;

use App\Facades\FileUploader;
use Illuminate\Foundation\Http\FormRequest;
use App\Models\Repositories\StudentRepository;

class Student extends FormRequest
{

    public function __construct(StudentRepository $studentRepository)
    {
        $this->studentRepository = $studentRepository;
    }
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "name"           => "required",
            "class"          => "required",
            "school"         => "required",
            "profile_pic"    => "sometimes", //IF WE HAVE TOUCHED THIS FIELDS THE ITS BECOME REQUIRED
            "batch"          => "required",
            "address"        => "required",
            "father"         => "required",
            "mother"         => "required",
            "father_contact" => "required",
        ];
    }

    public function messages(){
        return [
            'name.required'           => 'Student Name is required',
            'class.required'          => 'Student Class is required',
            'school.required'         => 'School Name is required',
            'batch.required'          => 'Batch Name is required',
            'address.required'        => 'Student Address is required',
            'father.required'         => 'Student Father Name is required',
            'mother.required'         => 'Student Mother Name is required',
            'father_contact.required' => 'Student Father Name is required' 

        ];
    }

    public function save($student = null)
    {
        $data = $this->validated();

        if($this->hasFile('profile_pic')){
            $data['profile_pic'] = FileUploader::setFileKey('profile_pic')->setMimeTypes(['image/jpeg', 'image/jpg', 'image/png', 'image/gif'])->setPath('student')->upload();

        }else{
            unset($data['profile_pic']);
        }

        return($student)
            ? $this->studentRepository->update($data,$student->id)
            : $this->studentRepository->create($data);
    }
}
