<?php

namespace App\Http\Requests;

use App\Facades\FileUploader;
use App\Models\Repositories\AnnouncementRepository;
use Illuminate\Foundation\Http\FormRequest;


class Announcement extends FormRequest
{

    public function __construct(AnnouncementRepository $announcementRepository)
    {
        $this->announcementRepository = $announcementRepository;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array 
     */
    public function rules()
    {
        return [
            "announcement" => "required",
            "image"        => "sometimes",
            "batch"        => "required",
            "class"        => "required",
            "attach"       => "sometimes"
        ];
    }

    public function messages(){
        return[
            "announcement.required" => 'Announcement Name is required',
            "batch.required"        => 'Batch Name is required',
            "attach.required"       => 'Enter a name for attached file'
        ];
    }

    public function save($announcement = null)
    { 
        
        $data = $this->validated();
        
        if($this->hasFile('image')){
            $data['image'] = FileUploader::setFileKey('image')->setMimeTypes(['image/jpeg', 'image/jpg', 'image/png', 'image/gif','application/pdf','application/msword','application/vnd.ms-excel'])->setFileName($data['attach'])->setPath('announcements')->upload();
        }else{
            unset($data['image']);
        }

        return($announcement)
            ? $this->announcementRepository->update($data,$announcement->id)
            : $this->announcementRepository->create($data);

        
    }
} 
