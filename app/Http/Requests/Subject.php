<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\Repositories\SubjectRepository;

class Subject extends FormRequest
{

    public function __construct(SubjectRepository $subjectRepository)
    {
        $this->subjectRepository = $subjectRepository;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "class"   => "required",
            "subject" => "required",
            "faculty" => "required"
        ];
    }

    public function save($subject = null)
    {
        $data = $this->validated();

        return($subject)
            ? $this->subjectRepository->update($data,$subject->id)
            : $this->subjectRepository->create($data);
    }
}
