<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\Repositories\UserRepository;

class User extends FormRequest
{

    public function __construct(UserRepository $userRepository){
        $this->userRepository = $userRepository;
    }
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "name" => "required"
        ];
    }

    public function save($user=null){
        $data = $this->validated();

        return($user)
            ? $this->userRepository->update($data , $user->id)
            : $this->userRepository->create($data);
    }   
}
