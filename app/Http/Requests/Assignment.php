<?php

namespace App\Http\Requests;

use App\Facades\FileUploader;
use App\Models\Repositories\AssignmentRepository;
use Illuminate\Foundation\Http\FormRequest;


class Assignment extends FormRequest
{

     public function __construct(AssignmentRepository $assignmentRepository)
    {
        $this->assignmentRepository = $assignmentRepository;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "assignment"      => "required",
            "topic"           => "required",
            "submission_date" => "required",
            "attachement"     => "sometimes",
            "attach_name"     => "sometimes",
            "classe"          => "required"
        ];
    }

    public function messages(){
        return[
            "assignment.required"      => "Assignment is Required",
            "topic.required"           => "Topic is Required",
            "submission_date.required" => "Submission Date is Required",
            "attach_name.required"     => "Enter a name for attached File",
            "classe.required"          =>  "Select Class to send assignment"
        ];
    }


    public function save($assignment = null)
    {
        $data = $this->validated();

        if($this->hasFile('attachement')){
            $data['attachement'] = FileUploader::setFileKey('attachement')->setMimeTypes(['image/jpeg', 'image/jpg', 'image/png', 'image/gif','application/pdf','application/msword','application/vnd.ms-excel'])->setFileName($data['attach_name'])->setPath('assignments')->upload();
        }else{
            unset($data['image']);
        }

        return($assignment)
            ? $this->assignmentRepository->update($data,$assignment->id)
            : $this->assignmentRepository->create($data);
    }
}
