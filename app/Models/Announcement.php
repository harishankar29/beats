<?php

namespace App\Models;



class Announcement extends Model
{

    protected $casts = [
		'class' => 'array',
		
    ];
    
    public $itemPerPage = 3;
    
    public function owner(){
        return $this->belongsTo(User::class , 'user_id' , 'id');
	}
	
	public function announceBatch()
	{
		return $this->hasMany('App\Models\AnnounceClass' , 'announce_id');
	}
}
