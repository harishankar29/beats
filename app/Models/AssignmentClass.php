<?php

namespace App\Models;



class AssignmentClass extends Model
{
    public function classes()
    {
        return $this->belongsTo(Classe::class , 'class_id' , 'id');
    }
}
