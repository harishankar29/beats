<?php

namespace App\Models;

use App\Models\Traits\ModelHelper;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model as Eloquent;

class Model extends Eloquent
{
	use  SoftDeletes, ModelHelper;
    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['id','created_by','created_at','updated_at','deleted_at','_token'];
}
