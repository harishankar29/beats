<?php

namespace App\Models\Traits;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Schema;

trait ModelHelper
{

	// here taken the table on which we are submitting data with getTable() method;
	/**
     * @return string
     */
    public function table()
    {
        return with(new static)->getTable();
    }

    // here taking out the coloumns of that table with getColumnListing(static::table()) method
    /**
     * return table columns
     * 
     * @return array
     */
    public function columns()
    {
        return Schema::getColumnListing(static::table());
    }

    // here taking out the specific coloumn with hasColumn(static::table(), $column) method;
	public function hasColumn($column)
    {
        return Schema::hasColumn(static::table(), $column);
    }

	public static function boot()
    {
        parent::boot();

        $user = Auth::user() ?? null;

        /**
         * Model creating event handle
         */
        static::creating(function ($model) use ($user) {

            if ($model->hasColumn('created_by') && $user) {
                $model->created_by = $user->id;
            }
        });


        /**
         * Model updating event handle
         */
        static::saving(function ($model) use ($user) {

            if ($model->hasColumn('updated_by') && $user) {
                $model->updated_by = $user->id;
            }
        });

        
        /**
         * Model deleting event handle
         */
        static::deleting(function ($model) use ($user) {

            if ($model->hasColumn('deleted_by') && $user) {
                $model->deleted_by = $user->id;
                try { //incase hard delete then it will throw model not found error
                    $model->update();
                } catch (\Exception $e) {
                }
            }
        });        

    }

}