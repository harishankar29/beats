<?php

namespace App\Models\Traits;

use Illuminate\Database\Schema\Blueprint;
// Blueprint is an open-source tool for rapidly generating multiple Laravel components from a single human-readable definition.

trait SchemaHelper{

	public function activityFields(Blueprint $table){
		$table->unsignedBigInteger('created_by')->nullable();
		$table->unsignedBigInteger('updated_by')->nullable();
		$table->unsignedBigInteger('deleted_by')->nullable();
	}
}

