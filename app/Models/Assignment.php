<?php

namespace App\Models;



class Assignment extends Model
{
   

    public $itemPerPage = 3;

    public function assignmentClass()
    {
        return $this->hasMany('App\Models\AssignmentClass', 'assignment_id' ,'id');
    }
}
