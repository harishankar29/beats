<?php

namespace App\Models\Repositories;

use App\Models\User;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Auth;

abstract class BaseRepository
{
    /**
     * User model instance hoder
     *
     * @var App/Models/User
     */
    protected $user = null;

    /**
     * Model instance holder
     *
     * @var [type]
     */
    protected $model = null;

    /**
     * Order by field holder
     *
     * @var string
     */
    protected $orderBy = 'updated_at';

    /**
     * Order direction holder
     *
     * @var string
     */
    protected $sort = 'desc';

    /**
     * Search key
     *
     * @var string
     */
    protected $searchKey = 'search';

    /**
     * Set relations to load
     *
     * @var array
     */
    protected $relations = [];

    /**
     * Select fields
     *
     * @var string
     */
    protected $select = null;

    /**
     * Search Fields
     *
     * @var array
     */
    protected $searchFields = null;

    /**
     * Sortable Fields
     *
     * @var array
     */
    protected $sortableFields = null;

    /**
     * Exported data holder
     *
     * @var array
     */
    protected $exports = [];

    /**
     * Inject Repository model
     */
    public function __construct()
    {
        $modelClass = str_replace(['Repository', 'Repositories\\'], '', get_class($this));

        $this->model = new $modelClass;

        $this->user = (Auth::check()) ? Auth::user() : null;
    }

    /**
     * Get repository model
     *
     * @return App\Models\Model
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * Get list sort direction
     *
     * @return string
     */
    public function getSort()
    {
        return $this->sort;
    }

    /**
     * Get Order by field
     *
     * @return string
     */
    public function getOrderBy()
    {
        return $this->orderBy;
    }

    /**
     * Get search key
     *
     * @return void
     */
    public function getSearchKey()
    {
        return $this->searchKey;
    }

    /**
     * Set orderBy instance
     * @param string $field
     *
     * @return mix
     */
    public function setOrderBy($field)
    {
        $this->orderBy = $field;

        return $this;
    }

    /**
     * Set sort field
     *
     * @param [type] $sort
     * @return void
     */
    public function setSort($sort)
    {
        $this->sort = $sort;

        return $this;
    }

    /**
     * Set user instance
     * @param App\Models\User $user
     *
     * @return mix
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Set model instance
     * @param mix $model
     *
     * @return mix
     */
    public function setModel($model)
    {
        $this->model = $model;

        return $this;
    }

    /**
     * Set item per page
     *
     * @param [type] $itemPerPage
     * @return void
     */
    public function setItemPerPage($itemPerPage)
    {
        $this->model->setPerPage($itemPerPage);

        return $this;
    }

    /**
     * Set eazer loading relations
     *
     * @param [type] $relations
     * @return void
     */
    public function with($relations)
    {
        if (is_string($relations)) {
            $relations = explode(',', $relations);
        }

        $this->relations = $relations;

        return $this;
    }

    /**
     * Set select for fetching list or an item
     *
     * @param [type] $select
     * @return void
     */
    public function setSelect($select)
    {
        $this->select = $select;

        return $this;
    }

    /**
     * Set Search Fields
     *
     * @param [type] $select
     * @return void
     */
    public function setSearchFields($searchFields)
    {
        $this->searchFields = $searchFields;

        return $this;
    }

    /**
     * Set Search Fields
     *
     * @param [type] $select
     * @return void
     */
    public function setSortableFields($sortableFields)
    {
        $this->sortableFields = $sortableFields;

        return $this;
    }

    /**
     * Get new query
     *
     * @return Builder
     */
    public function newQuery()
    {
        return $this->model->newQuery();
    }

    /**
     * Create account
     *
     * @param [type] $data
     * @return void
     */
    public function create($data)
    {
        //$data = $this->filterFieldAccess($data);

        if (in_array('user_id', $this->model->columns()) && !in_array('user_id', $data)) {
            $data['user_id'] = $this->user->id;
        }

        // if (in_array('account_id', $this->model->columns()) && !in_array('account_id', $data)) {
        //     $data['account_id'] = @$this->user->account->id;
        // }

        return $this->model->create($data);
    }

    /**
     * Update model
     *
     * @param [type] $data
     * @param [type] $id
     * @return void
     */
    public function update($data, $id)
    {
        // $data = $this->filterFieldAccess($data);

        $model = isset($this->model->id)
            ? $this->model
            : $this->model->where('id', $id)->firstOrFail();

        foreach ($data as $field => $value) {
            $model->{$field} = $value;
        }

        $model->save();

        return $model;
    }

    /**
     * Get model by id
     *
     * @param [type] $id
     * @return void
     */
    public function byId($id)
    {
        $query = $this->model->where('id', $id);

        if (count($this->relations) > 0) {
            $query->with($this->relations);
        } //set any ezer loading

        return $query->firstOrFail();
    }

   /**
     * List user model data
     *
     * @return void
     */
    public function list()
    {
        $builder = $this->model->orderBy('id','desc');

        return $this->get($builder);
    }

    /**
     * Repository get generic method
     *
     * @param mix $query
     * @return array
     */
    public function get($query = null)
    {
        $query = ($query) ?? $this->model->newQuery();

        $searchFields = $this->searchFields ?? $this->model->columns();

        $this->processOrderByAndSort();

        $query->when($this->select, function ($q, $select) {
            $q->select($select);
        });

        $query->when(request($this->searchKey, false), function ($q, $searchTerm) use ($searchFields) {
            $q->search($searchFields, $searchTerm);
        })->orderBy($this->orderBy, $this->sort);

        if (count($this->relations) > 0) {
            $query->with($this->relations);
        } //set any ezer loading

        $paginator = $query->paginate($this->model->itemPerPage);

        return [
            'paginator' => $paginator,
            'links' => $this->links($paginator),
            'perPage' => $this->model->getPerPage(),
            'searchKey' => $this->searchKey,
            'sortableFields' => $this->sortableFields($paginator)
        ];
    }

    /**
     * Generate paginator links
     *
     * @param LengthAwarePaginator $paginator
     * @return void
     */
    protected function links(LengthAwarePaginator $paginator)
    {
        if ($this->orderBy != $this->model->orderByField) {
            $paginator->appends(['orderBy' => $this->orderBy, 'sort' => $this->sort]);
        }

        if ($searchTerm = request($this->searchKey, false)) {
            $paginator->appends($this->searchKey, $searchTerm);
        }

        return $paginator->links();
    }

    /**
     * Create sortable fields column
     *
     * @param LengthAwarePaginator $paginator
     * @param array $fields
     * @return void
     */
    protected function sortableFields(LengthAwarePaginator $paginator)
    {
        $fields = $this->sortableFields ?? [];

        $fieldLinks = [];

        foreach ($fields as $field => $label) {
            $paginator->appends('orderBy', $field);

            $sort = request('sort') ?? ($this->sort ?? 'desc');

            $paginator->appends('sort', $sort);

            if ($searchTerm = request($this->searchKey, false)) {
                $paginator->appends($this->searchKey, $searchTerm);
            }

            $fieldLinks[] = [
                'label' => $label,
                'field' => $field,
                'link' => $paginator->total() > 0 ? $paginator->url(1) : '#'
            ];
        }

        return $fieldLinks;
    }

    /**
     * Set order by at time of fetching
     *
     * @param string $field
     * @param string $sort
     * @return void
     */
    protected function processOrderByAndSort()
    {
        $field = request('orderBy', '');

        $sort = request('sort', '');

        $this->orderBy = in_array($field, $this->model->columns())
            ? $field
            : ($this->model->orderByField ?? $this->orderBy);

        $this->sort = in_array(strtoupper($sort), ['ASC', 'DESC']) ? $sort : $this->sort;

        if (request('perPage', false)) {
            $this->setItemPerPage(request('perPage'));
        }
    }
}
