<?php

namespace App\Models\Repositories;

use App\Models\Announcement;
use App\Models\AnnounceClass;

Class AnnouncementRepository extends BaseRepository
{
	
	/* public function create($data){
		
		if (in_array('user_id', $this->model->columns()) && !in_array('user_id', $data)) {
            $data['user_id'] = $this->user->id;
		}
		
		$newAnnouncement = $this->model->create($data);
		
		foreach($data['batch'] as $bat){ 
			$newAnnounce = new AnnounceClass();
			$newAnnounce->announce_id = $newAnnouncement->id;
			$newAnnounce->class_id = $bat;
			$newAnnounce->save();
		}

		return $newAnnouncement; 
	}

	public function update($data,$id){  

		// $data = $this->filterFieldAccess($data);

        $model = isset($this->model->id)
            ? $this->model
            : $this->model->where('id', $id)->firstOrFail();

        foreach ($data as $field => $value) {
            $model->{$field} = $value;
        }

		$model->save();

        AnnounceClass::where('announce_id' , $model->id)->delete();
		foreach($data['batch'] as $bat){
			$newAnnounce = new AnnounceClass();
			$newAnnounce->announce_id =  $model->id;
			$newAnnounce->class_id = $bat;
			$newAnnounce->save();
		}

		return $model;
	} */
	
	public function student(){
		
		$list = $this->model->paginate(5);
		
		return $list;
	}
}