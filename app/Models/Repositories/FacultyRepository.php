<?php 

namespace App\Models\Repositories;

use App\Models\Faculty;

Class FacultyRepository extends BaseRepository
{
	public function student(){
		
		$list = $this->model->where('faculty_class', '=', $this->user->class)->paginate(5);
		
		return $list;
	}
}