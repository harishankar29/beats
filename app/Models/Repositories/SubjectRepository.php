<?php

namespace App\Models\Repositories;

use App\Models\Subject;

class SubjectRepository extends BaseRepository
{
    public function student(){
		
		$list = $this->model->where('class', '=', $this->user->class)->paginate(5);
		
		return $list;
	}
}