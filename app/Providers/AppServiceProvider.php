<?php

namespace App\Providers;


use Illuminate\Support\Facades\App;
use Illuminate\Support\ServiceProvider;
use Laravel\Sanctum\Sanctum;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        Sanctum::ignoreMigrations();
        $this->registerLocalFacades(); //File Uploader Register
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register local Facades
     *
     * @return void
     */
    public function registerLocalFacades()
    {
        App::bind('fileUploader', function () {
            return new \App\Http\Services\FileUploader;
        });
    }
}
