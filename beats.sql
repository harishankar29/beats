-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 14, 2020 at 01:50 PM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.4.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `beats`
--

-- --------------------------------------------------------

--
-- Table structure for table `announcements`
--

CREATE TABLE `announcements` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `announcement` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `batch` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `class` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attach` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `deleted_by` bigint(20) UNSIGNED DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `announcements`
--

INSERT INTO `announcements` (`id`, `user_id`, `announcement`, `image`, `batch`, `class`, `attach`, `created_by`, `updated_by`, `deleted_by`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 1, '1', NULL, '1', NULL, NULL, 1, 1, 1, '2020-09-11 00:37:09', '2020-09-10 04:03:24', '2020-09-11 00:37:09'),
(2, 2, '2', NULL, '1', NULL, NULL, 1, 1, 1, '2020-09-11 00:37:37', '2020-09-10 04:07:06', '2020-09-11 00:37:37'),
(3, 1, '1', NULL, '1', NULL, NULL, 1, 1, 1, '2020-09-11 00:39:10', '2020-09-11 00:38:58', '2020-09-11 00:39:10'),
(4, 1, '2', NULL, '2', NULL, NULL, 1, 1, 1, '2020-09-11 23:35:17', '2020-09-11 00:39:06', '2020-09-11 23:35:17'),
(5, 1, 'fvfsd', 'files/Btdgf.jpeg', '1', NULL, 'Btdgf', 1, 1, 1, '2020-09-11 23:31:18', '2020-09-11 23:09:47', '2020-09-11 23:31:18'),
(6, 1, 'fvfsd', '/storage/Btdgf.jpeg', '1', NULL, 'Btdgf', 1, 1, 1, '2020-09-11 23:37:31', '2020-09-11 23:16:28', '2020-09-11 23:37:31'),
(7, 1, 'fvfsd', '/storage/pdf.pdf', '2', NULL, 'pdf', 1, 1, 1, '2020-09-11 23:28:02', '2020-09-11 23:26:35', '2020-09-11 23:28:02'),
(8, 1, 'fvfsd', '/storage/Btdgf.jpeg', '1', NULL, 'Btdgf', 1, 1, NULL, NULL, '2020-09-11 23:49:32', '2020-09-11 23:49:32'),
(9, 1, 'q', NULL, '1', NULL, NULL, 1, 1, 1, '2020-09-12 00:43:12', '2020-09-12 00:04:56', '2020-09-12 00:43:12'),
(10, 1, 'fvfsd', '/storage/announcements/Btdgf.doc', '2', NULL, 'Btdgf', 1, 1, NULL, NULL, '2020-09-12 00:22:35', '2020-09-12 00:22:35'),
(11, 1, 'fvfsd', NULL, '1', NULL, 'Btdgf', 1, 1, NULL, NULL, '2020-09-12 04:39:41', '2020-09-12 04:39:41'),
(12, 1, 'fvfsd', NULL, '1', NULL, 'Btdgf', 1, 1, NULL, NULL, '2020-09-12 04:40:46', '2020-09-12 04:40:46'),
(13, 1, 'announcement', NULL, 'batch', NULL, 'attach', 1, 1, NULL, NULL, '2020-09-12 05:08:04', '2020-09-12 05:08:04'),
(14, 1, 'announcement', NULL, 'batch', '\"2\"', 'attach', 1, 1, NULL, NULL, '2020-09-12 05:18:28', '2020-09-12 05:18:28'),
(15, 1, 'announcement', NULL, 'batch', '\"1,2\"', 'attach', 1, 1, NULL, NULL, '2020-09-12 05:18:58', '2020-09-12 05:18:58'),
(16, 1, 'fvfsd', NULL, '1', '[\"9\",\"10\"]', 'Btdgf', 1, 1, 1, '2020-09-13 02:29:48', '2020-09-12 05:19:36', '2020-09-13 02:29:48'),
(17, 1, 'announcement', NULL, 'batch', '\"2\"', 'attach', 1, 1, NULL, NULL, '2020-09-12 05:56:51', '2020-09-12 05:56:51'),
(18, 1, 'fvfsd', NULL, '1', '[\"9\",\"10\"]', 'Btdgf', 1, 1, NULL, NULL, '2020-09-12 05:57:27', '2020-09-12 05:57:27'),
(19, 1, 'fvfsd', NULL, '1', '[\"9\",\"10\",\"11\",\"12\"]', 'Btdgf', 1, 1, NULL, NULL, '2020-09-12 05:58:49', '2020-09-12 05:58:49'),
(20, 1, 'fvfsd', NULL, '1', '[\"10\"]', 'Btdgf', 1, 1, NULL, NULL, '2020-09-13 00:52:53', '2020-09-13 00:52:53'),
(21, 1, 'qwerty', NULL, '1', '[\"10\",\"9\"]', 'Btdgf', 1, 1, 1, '2020-09-13 02:29:22', '2020-09-13 00:55:07', '2020-09-13 02:29:22'),
(22, 1, 'Ram', NULL, '1', '[2,1]', '3', 1, 1, 1, '2020-09-13 02:29:34', '2020-09-13 01:52:58', '2020-09-13 02:29:34'),
(23, 1, 'akash', NULL, '1', '[4,1]', 'Btdgf', 1, 1, NULL, NULL, '2020-09-13 02:27:01', '2020-09-13 23:39:31');

-- --------------------------------------------------------

--
-- Table structure for table `announce_classes`
--

CREATE TABLE `announce_classes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `announce_id` bigint(20) UNSIGNED DEFAULT NULL,
  `class_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `deleted_by` bigint(20) UNSIGNED DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `announce_classes`
--

INSERT INTO `announce_classes` (`id`, `announce_id`, `class_id`, `created_by`, `updated_by`, `deleted_by`, `deleted_at`, `created_at`, `updated_at`) VALUES
(2, 21, 1, 1, 1, NULL, NULL, '2020-09-13 00:55:07', '2020-09-13 00:55:07'),
(3, 21, 2, 1, 1, NULL, NULL, '2020-09-13 00:55:07', '2020-09-13 00:55:07'),
(4, 22, 2, 1, 1, NULL, NULL, '2020-09-13 01:52:59', '2020-09-13 01:52:59'),
(5, 22, 1, 1, 1, NULL, NULL, '2020-09-13 01:52:59', '2020-09-13 01:52:59'),
(6, 23, 2, 1, 1, NULL, '2020-09-13 23:31:51', '2020-09-13 02:27:03', '2020-09-13 23:31:51'),
(7, 23, 1, 1, 1, NULL, '2020-09-13 23:31:51', '2020-09-13 02:27:04', '2020-09-13 23:31:51'),
(8, 23, 4, 1, 1, NULL, '2020-09-13 23:39:32', '2020-09-13 23:37:24', '2020-09-13 23:39:32'),
(9, 23, 2, 1, 1, NULL, '2020-09-13 23:39:32', '2020-09-13 23:37:24', '2020-09-13 23:39:32'),
(10, 23, 4, 1, 1, NULL, NULL, '2020-09-13 23:39:32', '2020-09-13 23:39:32'),
(11, 23, 1, 1, 1, NULL, NULL, '2020-09-13 23:39:32', '2020-09-13 23:39:32');

-- --------------------------------------------------------

--
-- Table structure for table `assignments`
--

CREATE TABLE `assignments` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `assignment` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `topic` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `submission_date` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attachement` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `classe` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attach_name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `deleted_by` bigint(20) UNSIGNED DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `assignments`
--

INSERT INTO `assignments` (`id`, `assignment`, `topic`, `submission_date`, `attachement`, `classe`, `attach_name`, `created_by`, `updated_by`, `deleted_by`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, '2020-09-01 03:32:57', '2020-08-29 01:07:10', '2020-09-01 03:32:57'),
(2, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, '2020-09-01 03:32:55', '2020-08-29 01:10:52', '2020-09-01 03:32:55'),
(3, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, '2020-09-01 03:32:54', '2020-08-29 01:12:13', '2020-09-01 03:32:54'),
(4, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, '2020-09-01 03:32:53', '2020-08-29 01:16:21', '2020-09-01 03:32:53'),
(5, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, '2020-09-01 03:32:52', '2020-08-29 01:16:21', '2020-09-01 03:32:52'),
(6, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, '2020-09-01 03:32:50', '2020-08-29 01:17:47', '2020-09-01 03:32:50'),
(7, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, '2020-09-01 03:32:58', '2020-08-29 01:19:42', '2020-09-01 03:32:58'),
(8, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, '2020-09-01 03:33:00', '2020-08-29 01:21:22', '2020-09-01 03:33:00'),
(9, 'fvfsd', 'adf', '18-12-2019', NULL, NULL, NULL, 1, 1, 1, '2020-09-01 03:33:01', '2020-08-29 01:23:26', '2020-09-01 03:33:01'),
(10, 'fvfsd', 'adf', '18-12-2019', NULL, NULL, NULL, 1, 1, 1, '2020-09-01 03:33:02', '2020-08-29 01:24:31', '2020-09-01 03:33:02'),
(11, 'fvfsd', 'adf', '18-12-2019', NULL, NULL, NULL, 1, 1, 1, '2020-09-01 03:33:03', '2020-08-29 01:52:00', '2020-09-01 03:33:03'),
(12, 'fvfsd', 'adf', '18-12-2019', NULL, NULL, NULL, 1, 1, 1, '2020-09-01 03:33:04', '2020-09-01 03:20:45', '2020-09-01 03:33:04'),
(13, 'fvfsd', 'adf', '18-12-2019', NULL, NULL, NULL, 1, 1, 1, '2020-09-01 03:33:07', '2020-09-01 03:20:46', '2020-09-01 03:33:07'),
(14, 'Ram', 'adf', '18-12-2019', NULL, NULL, NULL, 1, 1, 1, '2020-09-01 03:33:08', '2020-09-01 03:21:58', '2020-09-01 03:33:08'),
(15, 'Ram', 'adf', '18-12-2019', NULL, NULL, NULL, 1, 1, 1, '2020-09-01 03:33:10', '2020-09-01 03:22:00', '2020-09-01 03:33:10'),
(16, 'Ram', 'adf', '18-12-2019', NULL, NULL, NULL, 1, 1, 1, '2020-09-01 03:27:45', '2020-09-01 03:23:08', '2020-09-01 03:27:45'),
(17, 'Ram', 'adf', '18-12-2019', NULL, NULL, NULL, 1, 1, 1, '2020-09-05 03:08:26', '2020-09-01 03:23:09', '2020-09-05 03:08:26'),
(18, 'Ram', 'adf', '18-12-2019', NULL, NULL, NULL, 1, 1, 1, '2020-09-01 03:32:45', '2020-09-01 03:23:09', '2020-09-01 03:32:45'),
(19, 'fvfsd222', 'adf', '02/19/2020', NULL, NULL, NULL, 1, 1, 1, '2020-09-01 03:27:49', '2020-09-01 03:23:36', '2020-09-01 03:27:49'),
(20, 'Ram', 'adf', '19-12-2019', NULL, NULL, NULL, 1, 1, 1, '2020-09-05 03:07:09', '2020-09-05 02:58:12', '2020-09-05 03:07:09'),
(21, 'fvfsd', 'adf', '02/19/2020', NULL, NULL, NULL, 1, 1, 1, '2020-09-05 03:08:31', '2020-09-05 03:05:05', '2020-09-05 03:08:31'),
(22, 'cddc', 'we12', '02/25/2020', NULL, NULL, NULL, 1, 1, 1, '2020-09-11 00:43:15', '2020-09-05 03:05:32', '2020-09-11 00:43:15'),
(23, 'cddcqq', 'adf', '02/19/2020', NULL, NULL, NULL, 1, 1, 1, '2020-09-11 00:43:12', '2020-09-05 03:08:20', '2020-09-11 00:43:12'),
(24, 'sasa', 'we', '02/25/2020', NULL, NULL, NULL, 1, 1, 1, '2020-09-11 00:43:19', '2020-09-05 06:20:37', '2020-09-11 00:43:19'),
(25, 'sasa', 'we', '02/25/2020', NULL, NULL, NULL, 1, 1, 1, '2020-09-12 00:38:32', '2020-09-05 06:20:38', '2020-09-12 00:38:32'),
(26, 'fvfsd', 'we', '00:13', NULL, NULL, 'pdf', 1, 1, 1, '2020-09-12 00:38:29', '2020-09-12 00:14:11', '2020-09-12 00:38:29'),
(27, 'eee', 'we', '2020-09-20', NULL, NULL, 'pdf', 1, 1, 1, '2020-09-12 00:38:25', '2020-09-12 00:18:42', '2020-09-12 00:38:25'),
(28, 'wqwe', 'q', '2020-09-17', NULL, NULL, 'www', 1, 1, 1, '2020-09-12 00:42:06', '2020-09-12 00:21:21', '2020-09-12 00:42:06'),
(29, 'Ram', 'q', '2020-09-25', '/storage/assignments/sss.pdf', NULL, 'sssqqq', 1, 1, NULL, NULL, '2020-09-12 00:37:00', '2020-09-12 00:41:59'),
(30, 'neew', 'new', '2020-09-17', NULL, '[5,2]', 'sss', 1, 1, NULL, NULL, '2020-09-14 02:07:46', '2020-09-14 02:07:46'),
(31, 'new', 'new', '2020-09-24', NULL, '[5,4,2]', 'bew', 1, 1, NULL, NULL, '2020-09-14 02:08:52', '2020-09-14 02:08:52'),
(32, 'qwe', 'qwe', '2020-09-18', NULL, '[5,4,2]', 'qwert', 1, 1, NULL, NULL, '2020-09-14 02:11:26', '2020-09-14 02:11:26'),
(33, 'new12', 'new', '2020-09-16', NULL, '[5,4]', 'bew', 1, 1, NULL, NULL, '2020-09-14 02:13:57', '2020-09-14 02:14:21'),
(34, 'fvfsd', 'we', '2020-09-19', '/storage/assignments/pou.pdf', '\"5,4,2\"', 'pou', 1, 1, NULL, NULL, '2020-09-14 05:52:43', '2020-09-14 05:52:43'),
(35, 'fvfsd', 'we', '2020-09-19', '/storage/assignments/pou.pdf', '\"5,4\"', 'pou', 1, 1, NULL, NULL, '2020-09-14 05:53:18', '2020-09-14 05:53:18'),
(36, 'fvfsd', 'we', '2020-09-11', '/storage/assignments/zzz.jpeg', '\"5,4,2\"', 'zzz', 1, 1, NULL, NULL, '2020-09-14 05:56:41', '2020-09-14 05:56:41'),
(38, 'fvfsdccc', 'we', '2020-09-05', NULL, '5', 'cccc', 1, 1, NULL, NULL, '2020-09-14 06:14:35', '2020-09-14 06:14:35'),
(39, 'fvfsdccc', 'we', '2020-09-05', NULL, '5', 'cccc', 1, 1, NULL, NULL, '2020-09-14 06:14:36', '2020-09-14 06:14:36'),
(40, 'akash', 'we', '2020-09-06', NULL, '9', 'aa', 1, 1, NULL, NULL, '2020-09-14 06:15:56', '2020-09-14 06:15:56');

-- --------------------------------------------------------

--
-- Table structure for table `assignment_classes`
--

CREATE TABLE `assignment_classes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `assignment_id` bigint(20) UNSIGNED DEFAULT NULL,
  `class_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `deleted_by` bigint(20) UNSIGNED DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `assignment_classes`
--

INSERT INTO `assignment_classes` (`id`, `assignment_id`, `class_id`, `created_by`, `updated_by`, `deleted_by`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 33, 5, 1, 1, NULL, '2020-09-14 02:14:21', '2020-09-14 02:13:57', '2020-09-14 02:14:21'),
(2, 33, 4, 1, 1, NULL, '2020-09-14 02:14:21', '2020-09-14 02:13:57', '2020-09-14 02:14:21'),
(3, 33, 2, 1, 1, NULL, '2020-09-14 02:14:21', '2020-09-14 02:13:58', '2020-09-14 02:14:21'),
(4, 33, 5, 1, 1, NULL, NULL, '2020-09-14 02:14:21', '2020-09-14 02:14:21'),
(5, 33, 4, 1, 1, NULL, NULL, '2020-09-14 02:14:21', '2020-09-14 02:14:21'),
(6, 37, 5, 1, 1, NULL, NULL, '2020-09-14 06:06:05', '2020-09-14 06:06:05');

-- --------------------------------------------------------

--
-- Table structure for table `batches`
--

CREATE TABLE `batches` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `batch` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `class` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subject` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `center` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `start_time` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `end_time` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `weekday` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_time` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `deleted_by` bigint(20) UNSIGNED DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `batches`
--

INSERT INTO `batches` (`id`, `batch`, `class`, `subject`, `center`, `date`, `start_time`, `end_time`, `weekday`, `created_time`, `created_by`, `updated_by`, `deleted_by`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'HARI SHANKAR1', '1', '1', '1', '2020-09-17', '14:38', '14:44', '3', NULL, 1, 1, 1, '2020-09-05 04:36:08', '2020-09-05 03:39:12', '2020-09-05 04:36:08'),
(2, 'HARI SHANKAR SINGHq', '1', '1', '1', '2020-09-26', '15:41', '15:42', '2', NULL, 1, 1, NULL, NULL, '2020-09-05 04:41:39', '2020-09-05 04:46:30'),
(3, 'HARI SHANKAR SINGH', '1', '1', '1', '2020-09-26', '15:41', '15:42', '2', NULL, 1, 1, 1, '2020-09-05 04:44:52', '2020-09-05 04:41:39', '2020-09-05 04:44:52'),
(4, 'sd', '1', '1', '1', '2020-09-05', '15:42', '15:42', '4', NULL, 1, 1, 1, '2020-09-05 04:46:17', '2020-09-05 04:42:47', '2020-09-05 04:46:17'),
(5, 'Birender Sahu', '4', '1', '1', '2020-09-12', '15:43', '15:48', '7', NULL, 1, 1, NULL, NULL, '2020-09-05 04:43:11', '2020-09-05 04:43:11'),
(6, 'vinod', '1', '1', '1', '2020-09-27', '15:48', '15:52', '6', NULL, 1, 1, NULL, NULL, '2020-09-05 04:46:12', '2020-09-05 04:46:12');

-- --------------------------------------------------------

--
-- Table structure for table `classes`
--

CREATE TABLE `classes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `class` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `class_status` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `deleted_by` bigint(20) UNSIGNED DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `classes`
--

INSERT INTO `classes` (`id`, `class`, `class_status`, `created_by`, `updated_by`, `deleted_by`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, '9', 'Active', NULL, NULL, NULL, NULL, NULL, NULL),
(2, '10', 'Active', NULL, NULL, NULL, NULL, NULL, NULL),
(4, '12', 'Active', 1, 1, NULL, NULL, '2020-09-13 23:23:04', '2020-09-13 23:23:04'),
(5, '9', 'InActive', 1, 1, NULL, NULL, '2020-09-14 00:02:29', '2020-09-14 00:02:29');

-- --------------------------------------------------------

--
-- Table structure for table `faculties`
--

CREATE TABLE `faculties` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `faculty_name` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `faculty_subject` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `faculty_class` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `deleted_by` bigint(20) UNSIGNED DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `faculties`
--

INSERT INTO `faculties` (`id`, `faculty_name`, `faculty_subject`, `faculty_class`, `created_by`, `updated_by`, `deleted_by`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'HARI SHANKAR SINGH', 'English', '9', 1, 1, NULL, NULL, '2020-09-11 00:18:34', '2020-09-11 00:18:34'),
(2, 'HARI SHANKAR SINGH', 'Maths', '9', 1, 1, 1, '2020-09-11 00:58:20', '2020-09-11 00:18:42', '2020-09-11 00:58:20'),
(3, 'HARI SHANKAR SINGH', 'maths34', '11', 1, 1, 1, '2020-09-11 00:58:16', '2020-09-11 00:18:51', '2020-09-11 00:58:16'),
(4, 'akash', 'maths1', '12', 1, 1, 1, '2020-09-11 00:57:53', '2020-09-11 00:19:05', '2020-09-11 00:57:53'),
(5, 'HARI SHANKAR SINGH', 'English', '121', 1, 1, NULL, NULL, '2020-09-11 02:31:33', '2020-09-11 02:31:33'),
(6, 'akash', 'English', '21', 1, 1, NULL, NULL, '2020-09-11 02:31:40', '2020-09-11 02:31:40');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `failed_jobs`
--

INSERT INTO `failed_jobs` (`id`, `connection`, `queue`, `payload`, `exception`, `failed_at`) VALUES
(14, 'database', 'default', '{\"uuid\":\"ce89b1a5-7a94-4ac7-afee-b98456e2d0f1\",\"displayName\":\"App\\\\Jobs\\\\NewRegisterEmail\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\NewRegisterEmail\",\"command\":\"O:25:\\\"App\\\\Jobs\\\\NewRegisterEmail\\\":9:{s:4:\\\"user\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:15:\\\"App\\\\Models\\\\User\\\";s:2:\\\"id\\\";i:1;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2020-09-10 07:09:17.059708\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:3:\\\"UTC\\\";}s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 'ArgumentCountError: Too few arguments to function App\\Mail\\RegisterUserMail::__construct(), 0 passed in C:\\xampp\\htdocs\\beats\\beats\\app\\Jobs\\NewRegisterEmail.php on line 35 and exactly 1 expected in C:\\xampp\\htdocs\\beats\\beats\\app\\Mail\\RegisterUserMail.php:21\nStack trace:\n#0 C:\\xampp\\htdocs\\beats\\beats\\app\\Jobs\\NewRegisterEmail.php(35): App\\Mail\\RegisterUserMail->__construct()\n#1 [internal function]: App\\Jobs\\NewRegisterEmail->handle()\n#2 C:\\xampp\\htdocs\\beats\\beats\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(37): call_user_func_array(Array, Array)\n#3 C:\\xampp\\htdocs\\beats\\beats\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\Util.php(37): Illuminate\\Container\\BoundMethod::Illuminate\\Container\\{closure}()\n#4 C:\\xampp\\htdocs\\beats\\beats\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(95): Illuminate\\Container\\Util::unwrapIfClosure(Object(Closure))\n#5 C:\\xampp\\htdocs\\beats\\beats\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(39): Illuminate\\Container\\BoundMethod::callBoundMethod(Object(Illuminate\\Foundation\\Application), Array, Object(Closure))\n#6 C:\\xampp\\htdocs\\beats\\beats\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\Container.php(596): Illuminate\\Container\\BoundMethod::call(Object(Illuminate\\Foundation\\Application), Array, Array, NULL)\n#7 C:\\xampp\\htdocs\\beats\\beats\\vendor\\laravel\\framework\\src\\Illuminate\\Bus\\Dispatcher.php(94): Illuminate\\Container\\Container->call(Array)\n#8 C:\\xampp\\htdocs\\beats\\beats\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(128): Illuminate\\Bus\\Dispatcher->Illuminate\\Bus\\{closure}(Object(App\\Jobs\\NewRegisterEmail))\n#9 C:\\xampp\\htdocs\\beats\\beats\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(103): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(App\\Jobs\\NewRegisterEmail))\n#10 C:\\xampp\\htdocs\\beats\\beats\\vendor\\laravel\\framework\\src\\Illuminate\\Bus\\Dispatcher.php(98): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#11 C:\\xampp\\htdocs\\beats\\beats\\vendor\\laravel\\framework\\src\\Illuminate\\Queue\\CallQueuedHandler.php(83): Illuminate\\Bus\\Dispatcher->dispatchNow(Object(App\\Jobs\\NewRegisterEmail), false)\n#12 C:\\xampp\\htdocs\\beats\\beats\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(128): Illuminate\\Queue\\CallQueuedHandler->Illuminate\\Queue\\{closure}(Object(App\\Jobs\\NewRegisterEmail))\n#13 C:\\xampp\\htdocs\\beats\\beats\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(103): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(App\\Jobs\\NewRegisterEmail))\n#14 C:\\xampp\\htdocs\\beats\\beats\\vendor\\laravel\\framework\\src\\Illuminate\\Queue\\CallQueuedHandler.php(85): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#15 C:\\xampp\\htdocs\\beats\\beats\\vendor\\laravel\\framework\\src\\Illuminate\\Queue\\CallQueuedHandler.php(59): Illuminate\\Queue\\CallQueuedHandler->dispatchThroughMiddleware(Object(Illuminate\\Queue\\Jobs\\DatabaseJob), Object(App\\Jobs\\NewRegisterEmail))\n#16 C:\\xampp\\htdocs\\beats\\beats\\vendor\\laravel\\framework\\src\\Illuminate\\Queue\\Jobs\\Job.php(98): Illuminate\\Queue\\CallQueuedHandler->call(Object(Illuminate\\Queue\\Jobs\\DatabaseJob), Array)\n#17 C:\\xampp\\htdocs\\beats\\beats\\vendor\\laravel\\framework\\src\\Illuminate\\Queue\\Worker.php(356): Illuminate\\Queue\\Jobs\\Job->fire()\n#18 C:\\xampp\\htdocs\\beats\\beats\\vendor\\laravel\\framework\\src\\Illuminate\\Queue\\Worker.php(306): Illuminate\\Queue\\Worker->process(\'database\', Object(Illuminate\\Queue\\Jobs\\DatabaseJob), Object(Illuminate\\Queue\\WorkerOptions))\n#19 C:\\xampp\\htdocs\\beats\\beats\\vendor\\laravel\\framework\\src\\Illuminate\\Queue\\Worker.php(132): Illuminate\\Queue\\Worker->runJob(Object(Illuminate\\Queue\\Jobs\\DatabaseJob), \'database\', Object(Illuminate\\Queue\\WorkerOptions))\n#20 C:\\xampp\\htdocs\\beats\\beats\\vendor\\laravel\\framework\\src\\Illuminate\\Queue\\Console\\WorkCommand.php(112): Illuminate\\Queue\\Worker->daemon(\'database\', \'default\', Object(Illuminate\\Queue\\WorkerOptions))\n#21 C:\\xampp\\htdocs\\beats\\beats\\vendor\\laravel\\framework\\src\\Illuminate\\Queue\\Console\\WorkCommand.php(96): Illuminate\\Queue\\Console\\WorkCommand->runWorker(\'database\', \'default\')\n#22 [internal function]: Illuminate\\Queue\\Console\\WorkCommand->handle()\n#23 C:\\xampp\\htdocs\\beats\\beats\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(37): call_user_func_array(Array, Array)\n#24 C:\\xampp\\htdocs\\beats\\beats\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\Util.php(37): Illuminate\\Container\\BoundMethod::Illuminate\\Container\\{closure}()\n#25 C:\\xampp\\htdocs\\beats\\beats\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(95): Illuminate\\Container\\Util::unwrapIfClosure(Object(Closure))\n#26 C:\\xampp\\htdocs\\beats\\beats\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(39): Illuminate\\Container\\BoundMethod::callBoundMethod(Object(Illuminate\\Foundation\\Application), Array, Object(Closure))\n#27 C:\\xampp\\htdocs\\beats\\beats\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\Container.php(596): Illuminate\\Container\\BoundMethod::call(Object(Illuminate\\Foundation\\Application), Array, Array, NULL)\n#28 C:\\xampp\\htdocs\\beats\\beats\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Command.php(134): Illuminate\\Container\\Container->call(Array)\n#29 C:\\xampp\\htdocs\\beats\\beats\\vendor\\symfony\\console\\Command\\Command.php(258): Illuminate\\Console\\Command->execute(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Illuminate\\Console\\OutputStyle))\n#30 C:\\xampp\\htdocs\\beats\\beats\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Command.php(121): Symfony\\Component\\Console\\Command\\Command->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Illuminate\\Console\\OutputStyle))\n#31 C:\\xampp\\htdocs\\beats\\beats\\vendor\\symfony\\console\\Application.php(911): Illuminate\\Console\\Command->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#32 C:\\xampp\\htdocs\\beats\\beats\\vendor\\symfony\\console\\Application.php(264): Symfony\\Component\\Console\\Application->doRunCommand(Object(Illuminate\\Queue\\Console\\WorkCommand), Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#33 C:\\xampp\\htdocs\\beats\\beats\\vendor\\symfony\\console\\Application.php(140): Symfony\\Component\\Console\\Application->doRun(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#34 C:\\xampp\\htdocs\\beats\\beats\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Application.php(93): Symfony\\Component\\Console\\Application->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#35 C:\\xampp\\htdocs\\beats\\beats\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Console\\Kernel.php(129): Illuminate\\Console\\Application->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#36 C:\\xampp\\htdocs\\beats\\beats\\artisan(37): Illuminate\\Foundation\\Console\\Kernel->handle(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#37 {main}', '2020-09-10 01:39:19'),
(15, 'database', 'default', '{\"uuid\":\"54253f4d-a54d-4b6d-86f6-cb1abbacfd4e\",\"displayName\":\"App\\\\Jobs\\\\NewRegisterEmail\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\NewRegisterEmail\",\"command\":\"O:25:\\\"App\\\\Jobs\\\\NewRegisterEmail\\\":9:{s:4:\\\"user\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:15:\\\"App\\\\Models\\\\User\\\";s:2:\\\"id\\\";i:1;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2020-09-10 07:12:51.407107\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:3:\\\"UTC\\\";}s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 'ArgumentCountError: Too few arguments to function App\\Mail\\RegisterUserMail::__construct(), 0 passed in C:\\xampp\\htdocs\\beats\\beats\\app\\Jobs\\NewRegisterEmail.php on line 35 and exactly 1 expected in C:\\xampp\\htdocs\\beats\\beats\\app\\Mail\\RegisterUserMail.php:21\nStack trace:\n#0 C:\\xampp\\htdocs\\beats\\beats\\app\\Jobs\\NewRegisterEmail.php(35): App\\Mail\\RegisterUserMail->__construct()\n#1 [internal function]: App\\Jobs\\NewRegisterEmail->handle()\n#2 C:\\xampp\\htdocs\\beats\\beats\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(37): call_user_func_array(Array, Array)\n#3 C:\\xampp\\htdocs\\beats\\beats\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\Util.php(37): Illuminate\\Container\\BoundMethod::Illuminate\\Container\\{closure}()\n#4 C:\\xampp\\htdocs\\beats\\beats\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(95): Illuminate\\Container\\Util::unwrapIfClosure(Object(Closure))\n#5 C:\\xampp\\htdocs\\beats\\beats\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(39): Illuminate\\Container\\BoundMethod::callBoundMethod(Object(Illuminate\\Foundation\\Application), Array, Object(Closure))\n#6 C:\\xampp\\htdocs\\beats\\beats\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\Container.php(596): Illuminate\\Container\\BoundMethod::call(Object(Illuminate\\Foundation\\Application), Array, Array, NULL)\n#7 C:\\xampp\\htdocs\\beats\\beats\\vendor\\laravel\\framework\\src\\Illuminate\\Bus\\Dispatcher.php(94): Illuminate\\Container\\Container->call(Array)\n#8 C:\\xampp\\htdocs\\beats\\beats\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(128): Illuminate\\Bus\\Dispatcher->Illuminate\\Bus\\{closure}(Object(App\\Jobs\\NewRegisterEmail))\n#9 C:\\xampp\\htdocs\\beats\\beats\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(103): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(App\\Jobs\\NewRegisterEmail))\n#10 C:\\xampp\\htdocs\\beats\\beats\\vendor\\laravel\\framework\\src\\Illuminate\\Bus\\Dispatcher.php(98): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#11 C:\\xampp\\htdocs\\beats\\beats\\vendor\\laravel\\framework\\src\\Illuminate\\Queue\\CallQueuedHandler.php(83): Illuminate\\Bus\\Dispatcher->dispatchNow(Object(App\\Jobs\\NewRegisterEmail), false)\n#12 C:\\xampp\\htdocs\\beats\\beats\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(128): Illuminate\\Queue\\CallQueuedHandler->Illuminate\\Queue\\{closure}(Object(App\\Jobs\\NewRegisterEmail))\n#13 C:\\xampp\\htdocs\\beats\\beats\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(103): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(App\\Jobs\\NewRegisterEmail))\n#14 C:\\xampp\\htdocs\\beats\\beats\\vendor\\laravel\\framework\\src\\Illuminate\\Queue\\CallQueuedHandler.php(85): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#15 C:\\xampp\\htdocs\\beats\\beats\\vendor\\laravel\\framework\\src\\Illuminate\\Queue\\CallQueuedHandler.php(59): Illuminate\\Queue\\CallQueuedHandler->dispatchThroughMiddleware(Object(Illuminate\\Queue\\Jobs\\DatabaseJob), Object(App\\Jobs\\NewRegisterEmail))\n#16 C:\\xampp\\htdocs\\beats\\beats\\vendor\\laravel\\framework\\src\\Illuminate\\Queue\\Jobs\\Job.php(98): Illuminate\\Queue\\CallQueuedHandler->call(Object(Illuminate\\Queue\\Jobs\\DatabaseJob), Array)\n#17 C:\\xampp\\htdocs\\beats\\beats\\vendor\\laravel\\framework\\src\\Illuminate\\Queue\\Worker.php(356): Illuminate\\Queue\\Jobs\\Job->fire()\n#18 C:\\xampp\\htdocs\\beats\\beats\\vendor\\laravel\\framework\\src\\Illuminate\\Queue\\Worker.php(306): Illuminate\\Queue\\Worker->process(\'database\', Object(Illuminate\\Queue\\Jobs\\DatabaseJob), Object(Illuminate\\Queue\\WorkerOptions))\n#19 C:\\xampp\\htdocs\\beats\\beats\\vendor\\laravel\\framework\\src\\Illuminate\\Queue\\Worker.php(132): Illuminate\\Queue\\Worker->runJob(Object(Illuminate\\Queue\\Jobs\\DatabaseJob), \'database\', Object(Illuminate\\Queue\\WorkerOptions))\n#20 C:\\xampp\\htdocs\\beats\\beats\\vendor\\laravel\\framework\\src\\Illuminate\\Queue\\Console\\WorkCommand.php(112): Illuminate\\Queue\\Worker->daemon(\'database\', \'default\', Object(Illuminate\\Queue\\WorkerOptions))\n#21 C:\\xampp\\htdocs\\beats\\beats\\vendor\\laravel\\framework\\src\\Illuminate\\Queue\\Console\\WorkCommand.php(96): Illuminate\\Queue\\Console\\WorkCommand->runWorker(\'database\', \'default\')\n#22 [internal function]: Illuminate\\Queue\\Console\\WorkCommand->handle()\n#23 C:\\xampp\\htdocs\\beats\\beats\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(37): call_user_func_array(Array, Array)\n#24 C:\\xampp\\htdocs\\beats\\beats\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\Util.php(37): Illuminate\\Container\\BoundMethod::Illuminate\\Container\\{closure}()\n#25 C:\\xampp\\htdocs\\beats\\beats\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(95): Illuminate\\Container\\Util::unwrapIfClosure(Object(Closure))\n#26 C:\\xampp\\htdocs\\beats\\beats\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(39): Illuminate\\Container\\BoundMethod::callBoundMethod(Object(Illuminate\\Foundation\\Application), Array, Object(Closure))\n#27 C:\\xampp\\htdocs\\beats\\beats\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\Container.php(596): Illuminate\\Container\\BoundMethod::call(Object(Illuminate\\Foundation\\Application), Array, Array, NULL)\n#28 C:\\xampp\\htdocs\\beats\\beats\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Command.php(134): Illuminate\\Container\\Container->call(Array)\n#29 C:\\xampp\\htdocs\\beats\\beats\\vendor\\symfony\\console\\Command\\Command.php(258): Illuminate\\Console\\Command->execute(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Illuminate\\Console\\OutputStyle))\n#30 C:\\xampp\\htdocs\\beats\\beats\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Command.php(121): Symfony\\Component\\Console\\Command\\Command->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Illuminate\\Console\\OutputStyle))\n#31 C:\\xampp\\htdocs\\beats\\beats\\vendor\\symfony\\console\\Application.php(911): Illuminate\\Console\\Command->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#32 C:\\xampp\\htdocs\\beats\\beats\\vendor\\symfony\\console\\Application.php(264): Symfony\\Component\\Console\\Application->doRunCommand(Object(Illuminate\\Queue\\Console\\WorkCommand), Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#33 C:\\xampp\\htdocs\\beats\\beats\\vendor\\symfony\\console\\Application.php(140): Symfony\\Component\\Console\\Application->doRun(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#34 C:\\xampp\\htdocs\\beats\\beats\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Application.php(93): Symfony\\Component\\Console\\Application->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#35 C:\\xampp\\htdocs\\beats\\beats\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Console\\Kernel.php(129): Illuminate\\Console\\Application->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#36 C:\\xampp\\htdocs\\beats\\beats\\artisan(37): Illuminate\\Foundation\\Console\\Kernel->handle(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#37 {main}', '2020-09-10 01:42:53'),
(16, 'database', 'default', '{\"uuid\":\"d770e4fa-723d-4578-a123-5dfb2efd3566\",\"displayName\":\"App\\\\Jobs\\\\NewRegisterEmail\",\"job\":\"Illuminate\\\\Queue\\\\CallQueuedHandler@call\",\"maxTries\":null,\"maxExceptions\":null,\"delay\":null,\"timeout\":null,\"timeoutAt\":null,\"data\":{\"commandName\":\"App\\\\Jobs\\\\NewRegisterEmail\",\"command\":\"O:25:\\\"App\\\\Jobs\\\\NewRegisterEmail\\\":9:{s:4:\\\"user\\\";O:45:\\\"Illuminate\\\\Contracts\\\\Database\\\\ModelIdentifier\\\":4:{s:5:\\\"class\\\";s:15:\\\"App\\\\Models\\\\User\\\";s:2:\\\"id\\\";i:2;s:9:\\\"relations\\\";a:0:{}s:10:\\\"connection\\\";s:5:\\\"mysql\\\";}s:3:\\\"job\\\";N;s:10:\\\"connection\\\";N;s:5:\\\"queue\\\";N;s:15:\\\"chainConnection\\\";N;s:10:\\\"chainQueue\\\";N;s:5:\\\"delay\\\";O:25:\\\"Illuminate\\\\Support\\\\Carbon\\\":3:{s:4:\\\"date\\\";s:26:\\\"2020-09-10 07:15:22.792019\\\";s:13:\\\"timezone_type\\\";i:3;s:8:\\\"timezone\\\";s:3:\\\"UTC\\\";}s:10:\\\"middleware\\\";a:0:{}s:7:\\\"chained\\\";a:0:{}}\"}}', 'ErrorException: Undefined variable: user in C:\\xampp\\htdocs\\beats\\beats\\app\\Jobs\\NewRegisterEmail.php:35\nStack trace:\n#0 C:\\xampp\\htdocs\\beats\\beats\\app\\Jobs\\NewRegisterEmail.php(35): Illuminate\\Foundation\\Bootstrap\\HandleExceptions->handleError(8, \'Undefined varia...\', \'C:\\\\xampp\\\\htdocs...\', 35, Array)\n#1 [internal function]: App\\Jobs\\NewRegisterEmail->handle()\n#2 C:\\xampp\\htdocs\\beats\\beats\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(37): call_user_func_array(Array, Array)\n#3 C:\\xampp\\htdocs\\beats\\beats\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\Util.php(37): Illuminate\\Container\\BoundMethod::Illuminate\\Container\\{closure}()\n#4 C:\\xampp\\htdocs\\beats\\beats\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(95): Illuminate\\Container\\Util::unwrapIfClosure(Object(Closure))\n#5 C:\\xampp\\htdocs\\beats\\beats\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(39): Illuminate\\Container\\BoundMethod::callBoundMethod(Object(Illuminate\\Foundation\\Application), Array, Object(Closure))\n#6 C:\\xampp\\htdocs\\beats\\beats\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\Container.php(596): Illuminate\\Container\\BoundMethod::call(Object(Illuminate\\Foundation\\Application), Array, Array, NULL)\n#7 C:\\xampp\\htdocs\\beats\\beats\\vendor\\laravel\\framework\\src\\Illuminate\\Bus\\Dispatcher.php(94): Illuminate\\Container\\Container->call(Array)\n#8 C:\\xampp\\htdocs\\beats\\beats\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(128): Illuminate\\Bus\\Dispatcher->Illuminate\\Bus\\{closure}(Object(App\\Jobs\\NewRegisterEmail))\n#9 C:\\xampp\\htdocs\\beats\\beats\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(103): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(App\\Jobs\\NewRegisterEmail))\n#10 C:\\xampp\\htdocs\\beats\\beats\\vendor\\laravel\\framework\\src\\Illuminate\\Bus\\Dispatcher.php(98): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#11 C:\\xampp\\htdocs\\beats\\beats\\vendor\\laravel\\framework\\src\\Illuminate\\Queue\\CallQueuedHandler.php(83): Illuminate\\Bus\\Dispatcher->dispatchNow(Object(App\\Jobs\\NewRegisterEmail), false)\n#12 C:\\xampp\\htdocs\\beats\\beats\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(128): Illuminate\\Queue\\CallQueuedHandler->Illuminate\\Queue\\{closure}(Object(App\\Jobs\\NewRegisterEmail))\n#13 C:\\xampp\\htdocs\\beats\\beats\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php(103): Illuminate\\Pipeline\\Pipeline->Illuminate\\Pipeline\\{closure}(Object(App\\Jobs\\NewRegisterEmail))\n#14 C:\\xampp\\htdocs\\beats\\beats\\vendor\\laravel\\framework\\src\\Illuminate\\Queue\\CallQueuedHandler.php(85): Illuminate\\Pipeline\\Pipeline->then(Object(Closure))\n#15 C:\\xampp\\htdocs\\beats\\beats\\vendor\\laravel\\framework\\src\\Illuminate\\Queue\\CallQueuedHandler.php(59): Illuminate\\Queue\\CallQueuedHandler->dispatchThroughMiddleware(Object(Illuminate\\Queue\\Jobs\\DatabaseJob), Object(App\\Jobs\\NewRegisterEmail))\n#16 C:\\xampp\\htdocs\\beats\\beats\\vendor\\laravel\\framework\\src\\Illuminate\\Queue\\Jobs\\Job.php(98): Illuminate\\Queue\\CallQueuedHandler->call(Object(Illuminate\\Queue\\Jobs\\DatabaseJob), Array)\n#17 C:\\xampp\\htdocs\\beats\\beats\\vendor\\laravel\\framework\\src\\Illuminate\\Queue\\Worker.php(356): Illuminate\\Queue\\Jobs\\Job->fire()\n#18 C:\\xampp\\htdocs\\beats\\beats\\vendor\\laravel\\framework\\src\\Illuminate\\Queue\\Worker.php(306): Illuminate\\Queue\\Worker->process(\'database\', Object(Illuminate\\Queue\\Jobs\\DatabaseJob), Object(Illuminate\\Queue\\WorkerOptions))\n#19 C:\\xampp\\htdocs\\beats\\beats\\vendor\\laravel\\framework\\src\\Illuminate\\Queue\\Worker.php(132): Illuminate\\Queue\\Worker->runJob(Object(Illuminate\\Queue\\Jobs\\DatabaseJob), \'database\', Object(Illuminate\\Queue\\WorkerOptions))\n#20 C:\\xampp\\htdocs\\beats\\beats\\vendor\\laravel\\framework\\src\\Illuminate\\Queue\\Console\\WorkCommand.php(112): Illuminate\\Queue\\Worker->daemon(\'database\', \'default\', Object(Illuminate\\Queue\\WorkerOptions))\n#21 C:\\xampp\\htdocs\\beats\\beats\\vendor\\laravel\\framework\\src\\Illuminate\\Queue\\Console\\WorkCommand.php(96): Illuminate\\Queue\\Console\\WorkCommand->runWorker(\'database\', \'default\')\n#22 [internal function]: Illuminate\\Queue\\Console\\WorkCommand->handle()\n#23 C:\\xampp\\htdocs\\beats\\beats\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(37): call_user_func_array(Array, Array)\n#24 C:\\xampp\\htdocs\\beats\\beats\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\Util.php(37): Illuminate\\Container\\BoundMethod::Illuminate\\Container\\{closure}()\n#25 C:\\xampp\\htdocs\\beats\\beats\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(95): Illuminate\\Container\\Util::unwrapIfClosure(Object(Closure))\n#26 C:\\xampp\\htdocs\\beats\\beats\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php(39): Illuminate\\Container\\BoundMethod::callBoundMethod(Object(Illuminate\\Foundation\\Application), Array, Object(Closure))\n#27 C:\\xampp\\htdocs\\beats\\beats\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\Container.php(596): Illuminate\\Container\\BoundMethod::call(Object(Illuminate\\Foundation\\Application), Array, Array, NULL)\n#28 C:\\xampp\\htdocs\\beats\\beats\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Command.php(134): Illuminate\\Container\\Container->call(Array)\n#29 C:\\xampp\\htdocs\\beats\\beats\\vendor\\symfony\\console\\Command\\Command.php(258): Illuminate\\Console\\Command->execute(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Illuminate\\Console\\OutputStyle))\n#30 C:\\xampp\\htdocs\\beats\\beats\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Command.php(121): Symfony\\Component\\Console\\Command\\Command->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Illuminate\\Console\\OutputStyle))\n#31 C:\\xampp\\htdocs\\beats\\beats\\vendor\\symfony\\console\\Application.php(911): Illuminate\\Console\\Command->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#32 C:\\xampp\\htdocs\\beats\\beats\\vendor\\symfony\\console\\Application.php(264): Symfony\\Component\\Console\\Application->doRunCommand(Object(Illuminate\\Queue\\Console\\WorkCommand), Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#33 C:\\xampp\\htdocs\\beats\\beats\\vendor\\symfony\\console\\Application.php(140): Symfony\\Component\\Console\\Application->doRun(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#34 C:\\xampp\\htdocs\\beats\\beats\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Application.php(93): Symfony\\Component\\Console\\Application->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#35 C:\\xampp\\htdocs\\beats\\beats\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Console\\Kernel.php(129): Illuminate\\Console\\Application->run(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#36 C:\\xampp\\htdocs\\beats\\beats\\artisan(37): Illuminate\\Foundation\\Console\\Kernel->handle(Object(Symfony\\Component\\Console\\Input\\ArgvInput), Object(Symfony\\Component\\Console\\Output\\ConsoleOutput))\n#37 {main}', '2020-09-10 01:45:23');

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE `jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `queue` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `attempts` tinyint(3) UNSIGNED NOT NULL,
  `reserved_at` int(10) UNSIGNED DEFAULT NULL,
  `available_at` int(10) UNSIGNED NOT NULL,
  `created_at` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(2, '2014_10_12_100000_create_password_resets_table', 1),
(4, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(5, '2020_08_02_053412_create_student_table', 1),
(7, '2020_08_02_053533_create_assignment_table', 1),
(8, '2020_08_02_053633_create_batch_table', 1),
(9, '2020_08_02_053719_create_class_table', 1),
(12, '2020_08_02_053905_create_static_page_table', 1),
(13, '2020_08_02_053835_create_faculty_table', 2),
(14, '2020_09_09_070120_create_jobs_table', 2),
(15, '2020_09_09_070523_create_failed_jobs_table', 2),
(16, '2014_10_12_000000_create_users_table', 3),
(20, '2020_08_02_053500_create_announcement_table', 4),
(21, '2020_08_02_053749_create_subject_table', 5),
(23, '2020_09_13_055330_announce__class', 6),
(25, '2020_09_14_055829_create_assignemt_class', 7);

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `banner1` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `banner2` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `deleted_by` bigint(20) UNSIGNED DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('akashsingh994@gmail.com', '$2y$10$S3YMdwH4eTgBHFcSBi.BHuGuOCME5bkPeL.WTKjKh09isByE4J8Ri', '2020-08-30 21:25:09');

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `students`
--

CREATE TABLE `students` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `class` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `school` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `profile_pic` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `batch` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `father` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mother` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `father_contact` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `deleted_by` bigint(20) UNSIGNED DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `students`
--

INSERT INTO `students` (`id`, `name`, `class`, `school`, `profile_pic`, `batch`, `address`, `father`, `mother`, `father_contact`, `created_by`, `updated_by`, `deleted_by`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'HARI SHANKAR SINGH', '9', 'DAV', NULL, '9', 'houseno 32 street 26 f molarband extn badrpur new delhi', 'sadasdsads', 'scdscscscsc', '9874563210', 1, 1, 1, '2020-08-28 23:55:50', '2020-08-23 20:34:22', '2020-08-28 23:55:50'),
(2, 'HARI SHANKAR SINGH', '9', 'DAV', NULL, '9', 'houseno 32 street 26 f molarband extn badrpur new delhi', 'sadasdsads', 'scdscscscsc123', '9874563210', 1, 1, 1, '2020-08-29 00:04:44', '2020-08-23 20:35:58', '2020-08-29 00:04:44'),
(3, 'HARI SHANKAR SINGH', '9', 'sfddfdsdssd', NULL, '9', 'houseno 32 street 26 f molarband extn badrpur new delhi', 'sadasdsads', 'scdscscscsc', '9874563210', 1, 1, 1, '2020-08-29 03:35:29', '2020-08-29 03:35:03', '2020-08-29 03:35:29'),
(4, 'HARI SHANKAR SINGH', '9', 'sfddfdsdssd', NULL, '9', 'houseno 32 street 26 f molarband extn badrpur new delhi', 'first', 'scdscscscsc', '9874563210', 1, 1, 1, '2020-09-01 01:20:00', '2020-08-30 01:06:47', '2020-09-01 01:20:00'),
(5, 'HARI SHANKAR SINGH', '9', 'sfddfdsdssd', NULL, '9', 'houseno 32 street 26 f molarband extn badrpur new delhi', 'sadasdsads', 'scdscscscsc', '9874563210', 1, 1, 1, '2020-09-01 01:07:14', '2020-08-30 01:20:42', '2020-09-01 01:07:14'),
(6, 'name', 'class', 'school', '/storage/images/ZILggB2rY8gURhR0METn1ZDtJ72YoA9QQzEN4l6T.png', 'batch', 'address', 'father', 'mother', 'father_contact', 1, 1, 1, '2020-09-01 01:07:13', '2020-08-31 23:18:07', '2020-09-01 01:07:13'),
(7, 'name', 'class', 'school', '/storage/images/Yo.png', 'batch', 'address', 'father', 'mother', 'father_contact', 1, 1, 1, '2020-09-01 01:07:12', '2020-08-31 23:19:07', '2020-09-01 01:07:12'),
(8, 'name', 'class', 'school', '/storage/student/Yo.png', 'batch', 'address', 'father', 'mother', 'father_contact', 1, 1, 1, '2020-09-01 01:07:11', '2020-08-31 23:21:42', '2020-09-01 01:07:11'),
(9, 'name', 'class', 'school', NULL, 'batch', 'address', 'father', 'mother', 'father_contact', 1, 1, 1, '2020-09-01 01:07:08', '2020-08-31 23:27:38', '2020-09-01 01:07:08'),
(10, 'name', 'class', 'school', '/storage/student/Yo.png', 'batch', 'address', 'father', 'mother', 'father_contact', 1, 1, 1, '2020-09-01 01:07:06', '2020-08-31 23:32:25', '2020-09-01 01:07:06'),
(11, 'name', 'class', 'school', '/storage/student/Yo.png', 'batch', 'address', 'father', 'mother', 'father_contact', 1, 1, 1, '2020-09-01 01:07:05', '2020-08-31 23:34:54', '2020-09-01 01:07:05'),
(12, 'name', 'class', 'school', '/storage/student/Yo.png', 'batch', 'address', 'father', 'mother', 'father_contact', 1, 1, 1, '2020-09-01 01:07:05', '2020-08-31 23:35:32', '2020-09-01 01:07:05'),
(13, 'name', 'class', 'school', '/storage/student/Yo.jpeg', 'batch', 'address', 'father', 'mother', 'father_contact', 1, 1, 1, '2020-09-01 01:07:03', '2020-08-31 23:36:07', '2020-09-01 01:07:03'),
(14, 'name', 'class', 'school', '/storage/student/Yo.png', 'batch', 'address', 'father', 'mother', 'father_contact', 1, 1, 1, '2020-09-01 01:07:03', '2020-09-01 00:04:36', '2020-09-01 01:07:03'),
(15, 'name', 'class', 'school', '/storage/student/Yo.png', 'batch', 'address', 'father', 'mother', 'father_contact', 1, 1, 1, '2020-09-01 01:07:02', '2020-09-01 00:07:00', '2020-09-01 01:07:02'),
(16, 'name', 'class', 'school', '/storage/student/uk0rG76qYByIqHmlq5hywsFsCgcfbvv8M3kI3Rz7.png', 'batch', 'address', 'father', 'mother', 'father_contact', 1, 1, 1, '2020-09-01 01:07:01', '2020-09-01 00:12:07', '2020-09-01 01:07:01'),
(17, 'name', 'class', 'school', '/storage/student/studentPicture.png', 'batch', 'address', 'father', 'mother', 'father_contact', 1, 1, 1, '2020-09-01 01:07:01', '2020-09-01 00:12:40', '2020-09-01 01:07:01'),
(18, 'name', 'class', 'school', '/storage/student/studentPicture.png', 'batch', 'address', 'father', 'mother', 'father_contact', 1, 1, 1, '2020-09-01 01:07:00', '2020-09-01 00:13:25', '2020-09-01 01:07:00'),
(19, 'HARI SHANKAR SINGH12', '9', 'sfddfdsdssd', '/storage/student/studentPicture.png', '9', 'houseno 32 street 26 f molarband extn badrpur new delhi', 'first', 'scdscscscsc', '9874563210', 1, 1, 1, '2020-09-01 01:06:56', '2020-09-01 01:03:22', '2020-09-01 01:06:56'),
(20, 'HARI SHANKAR SINGH', '9', 'DAV PUBLIC SCHOOL', '/storage/student/IDwo3TOjr0g3RLDoPJy4v2jXXU0xBwIzH9ATFTqO.jpeg', '9', 'houseno 32 street 26 f molarband extn badrpur new delhi', 'first', 'csdcsddsv', '9874563210', 1, 1, 1, '2020-09-05 00:54:52', '2020-09-01 01:20:58', '2020-09-05 00:54:52'),
(21, 'HARI SHANKAR SINGH', '9', 'DPS PUBLIC SCHOOL', '/storage/student/IDwo3TOjr0g3RLDoPJy4v2jXXU0xBwIzH9ATFTqO.jpeg', '9', 'houseno 32 street 26 f molarband extn badrpur new delhi', 'first', 'csdcsddsv', '9874563210', 1, 1, 1, '2020-09-05 00:51:06', '2020-09-01 01:20:58', '2020-09-05 00:51:06'),
(22, 'HARI SHANKAR SINGH', '9', 'DAV PUBLIC SCHOOL', '/storage/student/IDwo3TOjr0g3RLDoPJy4v2jXXU0xBwIzH9ATFTqO.jpeg', '9', 'houseno 32 street 26 f molarband extn badrpur new delhi', 'first', 'csdcsddsv', '9874563210', 1, 1, 1, '2020-09-05 00:51:05', '2020-09-01 01:20:58', '2020-09-05 00:51:05'),
(23, 'HARI SHANKAR SINGH', '9', 'DPS PUBLIC SCHOOL', '/storage/student/IDwo3TOjr0g3RLDoPJy4v2jXXU0xBwIzH9ATFTqO.jpeg', '9', 'houseno 32 street 26 f molarband extn badrpur new delhi', 'first', 'csdcsddsv', '9874563210', 1, 1, 1, '2020-09-05 00:51:04', '2020-09-01 01:20:58', '2020-09-05 00:51:04'),
(24, 'HARI SHANKARww', '9', 'DPS PUBLIC SCHOOL', '/storage/student/yIMKah6W3NNLnX9rlLhEmKev8ECRhtqteJHctQ7s.jpeg', '9', 'houseno 32 street 26 f molarband extn badrpur new delhi', 'first', 'csdcsddsv', '9874563210', 1, 1, 1, '2020-09-05 00:51:02', '2020-09-05 00:37:17', '2020-09-05 00:51:02'),
(25, 'HARI SHANKAR SINGH', '9', 'sfddfdsdssd', '/storage/student/dXDu8dDZFAvYs4KhWm7OWheWH3Je66jdeWxJwOKS.jpeg', '9', 'h32', 'first', 'scdscscscsc', '9874563210', 1, 1, 1, '2020-09-05 01:07:04', '2020-09-05 01:06:08', '2020-09-05 01:07:04'),
(26, 'HARI SHANKAR SINGH', '9', 'sfddfdsdssd', '/storage/student/gCHgOCUBJEfg8ETh4skqIDRPtOTDV0TNTZRJgZP6.jpeg', '9', '12', 'first', 'scdscscscsc', '9874563210', 1, 1, 1, '2020-09-05 02:08:48', '2020-09-05 01:36:09', '2020-09-05 02:08:48'),
(27, 'HARI SHANKAR SINGH', '9', 'gd', '/storage/student/DtjDIrq4rMtYOFfC4egwsG5gFBZMSm6Ix50LcDjq.jpeg', '9', 'h32', 'first', 'sdcdscsdcs', '9874563210', 1, 1, 1, '2020-09-11 23:37:06', '2020-09-05 01:48:58', '2020-09-11 23:37:06'),
(28, 'HARI SHANKAR SINGH', '9', 'sfddfdsdssd', '/storage/student/soiqu7vST9nfGWylaAkZ1MdBUwqugjhWVtmhZmaO.jpeg', '9', '12', 'sadasdsads', 'scdscscscsc', '9874563210', 1, 1, 1, '2020-09-11 00:33:08', '2020-09-05 01:49:47', '2020-09-11 00:33:08'),
(29, 'HARI SHANKAR SINGH', '9', 'sfddfdsdssd', '/storage/student/HsLAQxDRNlKrS0heLRFkjRogiivrSwJ41UEfx8WL.jpeg', '9', '12', 'first', 'sdcdscsdcs', '9874563210', 1, 1, 1, '2020-09-11 00:33:05', '2020-09-05 01:50:37', '2020-09-11 00:33:05'),
(30, 'l', '11', 'l', '/storage/student/WcjchmElJj9hImhM9Da99otqfkW8X7xumJCrGMKb.jpeg', '9', 'l', 'l', 'l', 'l', 1, 1, 1, '2020-09-11 00:32:35', '2020-09-05 02:24:40', '2020-09-11 00:32:35'),
(31, 'HARI SHANKAR SINGH', '9', 'gd', NULL, '9', '12', 'sadasdsads', 'scdscscscsc', '9874563210', 1, 1, 1, '2020-09-05 06:44:09', '2020-09-05 02:27:22', '2020-09-05 06:44:09'),
(32, 'HARI SHANKAR SINGH', '9', 'sfddfdsdssd', '/storage/student/wNRkqCNfwYoAEkt4gQbbpaTWgahMrIgfLRfnqiif.pdf', '9', '12', 'first', 'scdscscscsc', '9874563210', 1, 1, 1, '2020-09-05 07:25:22', '2020-09-05 07:19:48', '2020-09-05 07:25:22'),
(33, 'akash', '10', 'q', NULL, '9', 'q', 'q', 'q', 'q', 1, 1, 1, '2020-09-07 00:21:28', '2020-09-07 00:21:21', '2020-09-07 00:21:28'),
(34, 'akash', '10', 'sdscd', NULL, '9', 'h32', 'first', 'scdscscscsc', 'q', 1, 1, 1, '2020-09-11 00:33:36', '2020-09-11 00:33:25', '2020-09-11 00:33:36'),
(35, 'akash', '10', 'sfddfdsdssd', '/storage/student/Awj6E6fR5kc9A1YLRDfkSq6P7qF2wNfbgcraaQyX.pdf', '9', '12', 'scscsdcdsc', 'scdscscscsc', 'l', 1, 1, 1, '2020-09-11 07:08:59', '2020-09-11 06:53:03', '2020-09-11 07:08:59'),
(36, 'HARI SHANKAR SINGH', '9', 'sfddfdsdssd', '/storage/student/KicSpTMo6DuYSi93b7Sz2SQZEbi2tVLFmnmDGr6l.jpeg', '9', 'houseno 32 street 26 f molarband extn badrpur new delhi', 'first', 'scdscscscsc', '9874563213', 1, 1, NULL, NULL, '2020-09-14 05:52:03', '2020-09-14 05:52:03');

-- --------------------------------------------------------

--
-- Table structure for table `subjects`
--

CREATE TABLE `subjects` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `class` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subject` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `faculty` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `deleted_by` bigint(20) UNSIGNED DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `subjects`
--

INSERT INTO `subjects` (`id`, `class`, `subject`, `faculty`, `created_by`, `updated_by`, `deleted_by`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, '1', 'maths', 'faclt.faculty_name', 1, 1, 1, '2020-09-11 02:57:24', '2020-09-11 02:00:47', '2020-09-11 02:57:24'),
(2, '2', 'maths', 'HARI SHANKAR SINGH', 1, 1, 1, '2020-09-11 02:57:28', '2020-09-11 02:01:54', '2020-09-11 02:57:28'),
(3, '1', 'maths1', 'HARI SHANKAR SINGH', 1, 1, 1, '2020-09-11 02:57:19', '2020-09-11 02:23:30', '2020-09-11 02:57:19'),
(4, '9', 'maths1', 'akash', 1, 1, NULL, NULL, '2020-09-11 02:23:53', '2020-09-11 03:49:05'),
(5, '9', 'maths', 'HARI SHANKAR SINGH', 1, 1, 1, '2020-09-11 02:57:02', '2020-09-11 02:24:45', '2020-09-11 02:57:02'),
(6, '10', 'English', 'HARI SHANKAR SINGH', 1, 1, 1, '2020-09-11 02:56:10', '2020-09-11 02:26:06', '2020-09-11 02:56:10'),
(7, '10', 'English', 'akash', 1, 1, 1, '2020-09-11 02:54:26', '2020-09-11 02:43:12', '2020-09-11 02:54:26'),
(8, '10', 'English', 'akash', 1, 1, NULL, NULL, '2020-09-11 04:49:28', '2020-09-11 04:49:28');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `class` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `role`, `class`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'akashsingh994@gmail.com', 'admin', NULL, '2020-09-10 01:42:14', '$2y$10$m/DylVUbCa4ar8Lp/crBg.RFs2Zf/0zDlIJZogFeZb.cw7HjoAVvu', NULL, '2020-09-10 01:41:51', '2020-09-10 01:42:14'),
(2, 'Student', 'akashsingh000@yahoo.in', 'student', '9', '2020-09-10 01:44:41', '$2y$10$dNdx1h3XMsPaNjr.1npQO.R9HQPPxBz5hDW8vI.w6JdNjNY7GZg3K', NULL, '2020-09-10 01:44:22', '2020-09-10 01:44:41');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `announcements`
--
ALTER TABLE `announcements`
  ADD PRIMARY KEY (`id`),
  ADD KEY `announcements_user_id_foreign` (`user_id`);

--
-- Indexes for table `announce_classes`
--
ALTER TABLE `announce_classes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `assignments`
--
ALTER TABLE `assignments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `assignment_classes`
--
ALTER TABLE `assignment_classes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `batches`
--
ALTER TABLE `batches`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `classes`
--
ALTER TABLE `classes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `faculties`
--
ALTER TABLE `faculties`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jobs_queue_index` (`queue`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `students`
--
ALTER TABLE `students`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subjects`
--
ALTER TABLE `subjects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `announcements`
--
ALTER TABLE `announcements`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `announce_classes`
--
ALTER TABLE `announce_classes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `assignments`
--
ALTER TABLE `assignments`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `assignment_classes`
--
ALTER TABLE `assignment_classes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `batches`
--
ALTER TABLE `batches`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `classes`
--
ALTER TABLE `classes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `faculties`
--
ALTER TABLE `faculties`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `jobs`
--
ALTER TABLE `jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `students`
--
ALTER TABLE `students`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `subjects`
--
ALTER TABLE `subjects`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `announcements`
--
ALTER TABLE `announcements`
  ADD CONSTRAINT `announcements_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
